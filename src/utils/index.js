import { createContext, useState } from "react"
import { InputAddress } from "ui/InputAddress"

export const setFormData = (formData, field, value) => {
    formData.append(field, value)
}



export function GetYoutubeVideoImg(url,) {
    let size = 'small'
    let results = null
    let video = null

    if (url === null) {
        return ''
    }

    size = (size === null) ? 'big' : size;
    results = url.match('[\\?&]v=([^&#]*)');
    video = (results === null) ? url : results[1];

    if (size === 'small') {
        return 'http://img.youtube.com/vi/' + video + '/2.jpg';
    }
    return 'http://img.youtube.com/vi/' + video + '/0.jpg';
}


export const getVideoImg = (url) => {
    const result = url.match(/http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)/)

    return `https://img.youtube.com/vi/${result ? result[1] : null}/hqdefault.jpg`
}


export const appErros = {
    ru: {
        'These credentials do not match our records.': 'Эти данные не соответствуют требованиям',
        'The password confirmation does not match.': 'Пароли не совпадают',
        'The email has already been taken.': 'Такой email уже существует',
        'The banners_pictures.0 must be a file of type: jpg, jpeg, png, bmp.': 'Фон банера должен быть формата jpg, jpeg, png, bmp',
        'The password confirmation does not match.': 'Пароли не совпадают',
        'Your email address is not verified.': 'Вам на почту отправленно письмо для подтверждения email',
        'Please wait before retrying.': 'Попробуйте позже',
        'We have emailed your password reset link!': 'Мы отправили вам ссылку с сбросом пароля',
        'The slug has already been taken.': 'Такой адрес уже существует',
        'The slug already exists.': 'Такой адрес уже существует',
        'The email must be a valid email address.':'Email должен быть валидным',
        'The password confirmation does not match.':'Пароли не совпадают',
        'This password reset token is invalid.':'Сыллка сброса пароля устарела',
        'verification-link-sent':'На ваше почту отправленно письмо с сылкой'
    }
}



export const MainFormContext = createContext(null)