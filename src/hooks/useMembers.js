import { useState } from "react"
import { v1 } from "uuid"

export function useMembers() {

    const [members, setMembers] = useState([])

    const addMembers = items => setMembers(items)

    const addMember = () => {
        const exampel = {
            id: v1(),
            job: '',
            name: '',
            link: '',
            error: false
        }
        setMembers(prev => [...prev, exampel])
    }

    const removeMember = id => () => setMembers(prev => prev.filter(member => member.id !== id))

    const regexp = /^(http|https):\/\/.+$/

    const setMemberField = (index, field, type, error) => e => {
        setMembers(prev => Object.values({
            ...prev,
            [index]: {
                ...prev[index],
                [field]: e.target.value,
                error: field === 'link' ? (e.target.value.length && !regexp.test(String(e.target.value).toLowerCase())) : false
            }
        }))

        if (field === 'link') {
            if (!regexp.test(String(e.target.value).toLowerCase()) && e.target.value.length) {
                error('Ссылка должна содержать http(s)://')
                return
            }
            error(null)
        }
    }

    return {
        members,
        addMembers,
        addMember,
        removeMember,
        setMemberField
    }
}