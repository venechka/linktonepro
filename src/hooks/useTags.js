import { useState } from "react"
import { v1 } from "uuid"


export function useTags() {

    const regexp = /[\d\wа-яА-ЯёЁ]+/
    const [tags, setTags] = useState([])

    const addTags = items => setTags(items)

    const addTag = e => {
        const hasTag = tags.find(t => t.title === e.target.value)

        if (tags.length < 5 && regexp.test(String(e.target.value).toLowerCase()) && !hasTag) {
            const tag = {
                id: v1(),
                title: e.target.value,
            }
            setTags(prev => [...prev, tag])
            e.target.value = ''
            return
        }
        e.target.value = ''
    }

    const removeTag = id => () => setTags(prev => prev.filter(t => t.id !== id))

    const getTagsValue = () => tags.map(_tag => _tag.title)

    return {
        tags,
        addTags,
        addTag,
        removeTag,
        getTagsValue
    }

}