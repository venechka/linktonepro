import { useFormik } from 'formik'
import { useState } from 'react'
import * as Yup from 'yup'


export function useArtistSchema(artist_types, genres, slug) {

    const schema = Yup.object().shape({
        name: Yup.string()
            .required('Введите название артсита')
            
            .label('Название артиста'),
        slug: Yup.string()
            .meta({
                inputAddress: true,
                artist_slug: slug
            })
            .label('Адрес профиля')
            .required('Укажите ссылку на профиль')
            .test('onluEng', 'Некорректные символы', (value) => {
                const regex = /^[a-zA-Z0-9\-]{3,}$/
                return regex.test(String(value).toLowerCase())
            })
            .nullable(),
        artist_types_id: Yup.string()
            .meta({
                select: true,
                dropDownItems: artist_types
            })
            .required('Установите тип артиста')
            .label('Тип артиста'),
        genres_id: Yup.string()
            .meta({
                select: true,
                dropDownItems: genres
            })
            .required('Установите жанр артиста')
            .label('Стиль артиста'),
        country: Yup.string()
            .required('Введите страну')
            .meta({
                country: true,
                route: '/api/app/searchCountry',
                type: 'country'
            })
            .label('Страна'),
        city: Yup.string()
            .required('Введите город')
            .meta({
                city: true,
                route: '/api/app/searchCity',
                type: 'city',
                // country:
            })
            .label('Населенный пункт'),

        description: Yup.string()
            .meta({
                textArea: true
            })
            .label('Биография артиста\n(max 500 символов)')
            .nullable(),
    })

    return {
        schema,
    }
}