import { $api } from "api"
import { useRouter } from "next/router";

export function useRequest() {

    const router = useRouter()

    const set = async (route, formData, redirect) => {
        try {
            await $api.post(route, formData)
            router.push(redirect)
            return undefined
        } catch (error) {
            switch (error.response.status) {
                case 422:
                    return `${Object.values(error.response.data.errors)[0]}`
                // case 403:
                //     router.push('/403')
                //     break
                case 404:
                    router.push('/')
                    break
                case 500:
                    return 'Ошибка сервера'
                default:
                    return 
            }
        }
        // const response = await $api.post(route, formData)
        //     .then(res => res)
        //     .catch(res => res.response)
    }

    const get = async () => {
    }

    return {
        get,
        set
    }
}