import { useState } from "react";

export function useEssenceInputs({ essence }) {
    const [inputs, setInputs] = useState({
        artist: {
            // Component:
        },
        release: {

        },
        video: {

        }
    })

    return {
        inputs: inputs[essence]
    }
}