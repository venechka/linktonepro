import * as Yup from 'yup'


export function useReleaseSchema(release_types, slug) {


    const schema = Yup.object().shape({
        name: Yup.string()
            .required('Введите название релиза')
            .meta({
                maxLength: 50
            })
            .label('Название релиза\n(max 50)'),
        slug: Yup.string()
            .meta({
                inputAddress: true,
                artist_slug: slug
            })
            .test('onluEng', 'Некорректные символы', (value) => {
                const regex = /^[a-zA-Z0-9\-]{3,}$/
                return regex.test(String(value).toLowerCase())
            })
            .label('Адрес релиза')
            .required('Укажите адрес релиза')
            .nullable(),
        release_type_id: Yup.string()
            .meta({
                select: true,
                dropDownItems: release_types
            })
            .required('Выберите тип релиза')
            .label('Тип релиза'),
        publish_date: Yup.object()
            .meta({
                dateTime: true
            })
            .shape({
                date: Yup.string().required(),
                time: Yup.string(),
            }),
        label_company: Yup.string()
            .required('Выпускающая компания')
            .label('Выпускающая компания'),
        tracks: Yup.string()
            .meta({
                textArea: true
            })
            .label('Трек-лист \n (каждый трек с новой строки)')
            .nullable(),
    })

    return {
        schema
    }
}