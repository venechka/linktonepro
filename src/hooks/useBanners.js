import { useState } from "react"
import { v1 } from "uuid"

export function useBanners() {

    const [banners, setBanners] = useState([])
    const [deletedBanners, setDeletedBanners] = useState([])

    const addBanners = items => {
        setBanners(prev => items)
    }

    const addBanner = () => {
        const banner = {
            id: v1(),
            name: '',
            link: '',
            file: null,
            upload: null,
            wasDeleted: false,
            isChanged: false
        }
        if (banners.length !== 3) setBanners(prev => [...prev, banner])
    }

    const removeBanner = id => () => {
        setBanners(prev => prev.filter(banner => banner.id !== id))
        if (+id) setDeletedBanners(prev => [...prev, { id, delete: true }])
    }

    const setBannerField = (index, field) => e => setBanners(prev => {
        let file = null

        const type = field === 'upload'
        if (e && type) {
            file = e.target.files[0]
        }

        return Object.values({
            ...prev,
            [index]: {
                ...prev[index],
                [field]: type ? file : e?.target?.value,
                isChanged: true,
            }
        })
    })

    return {
        banners,
        addBanners,
        addBanner,
        removeBanner,
        setBannerField,
        deletedBanners
    }
}