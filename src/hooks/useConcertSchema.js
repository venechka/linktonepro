import * as Yup from 'yup'

export function useConcertSchema() {
    const schema = Yup.object().shape({
        publish_date: Yup.object()
            .meta({
                dateTime: true,
                concert: true
            })
            .shape({
                date: Yup.string().required(),
                time: Yup.string(),
            })
            .label('Дата и время начала концерта'),
        // slug: Yup.string()
        //     .meta({
        //         inputAddress: true,
        //         artist_slug: artist ? artist.slug : ''
        //     })
        //     .test('onluEng', 'Некорректные символы', (value) => {
        //         const regex = /^[a-zA-Z0-9\-]{5,}$/
        //         return regex.test(String(value).toLowerCase())
        //     })
        //     .label('Адрес концерта')
        //     .required('Укажите адрес концерта'),
        ticket_links: Yup.string()
            .required('Укажите ссылку на билеты')
            .test('checkLink', 'Неверная ссылка', (value) => {
                if (!regexp.test(String(value).toLowerCase())) {
                    setUnProcessableContent('Ссылка должна содержать http(s)://')
                    return false
                }
                setUnProcessableContent(null)
                return true
            })
            .label('Ссылка на билеты'),
        youtubeLink: Yup.string()
            .notRequired()
            .meta({
                youtubeLink: true,
                isConcert: true
            })
            .nullable()
            .label('Ссылка на видео'),
        country: Yup.string()
            .required('Введите страну')
            .meta({
                country: true,
                route: '/api/app/searchCountry',
                type: 'country'
            })
            .label('Страна'),
        city: Yup.string()
            .required('Введите город')
            .meta({
                city: true,
                route: '/api/app/searchCity',
                type: 'city',
            })
            .label('Город'),
        venue: Yup.string()
            .required('Введите площадку')
            .label('Площадка'),
        type: Yup.string()
            .meta({
                select: true,
                dropDownItems: app ? app.dictionaries.concert_types : []
            })
            .required('Выбирите тип выступления')
            .label('Тип выступления'),
    })

    return {
        schema
    }
}