import { useEffect, useState } from "react";

export function useLoading(esence) {

    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        setIsLoading(false)
    }, [esence])
    
    return {
        isLoading
    }
}