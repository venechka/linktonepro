import useSWR from 'swr'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { $api } from 'api'
import { appErros } from 'utils'


export const useAuth = ({ middleware, redirectIfAuthenticated } = {}) => {
    const router = useRouter()

    const [status, setStatus] = useState(null)
    const [verifyError, setVerifyError] = useState(null)


    const { data: app, error, mutate } = useSWR('/api/app/get', () =>
        $api
            .get('/api/app/get')
            .then(res => {
                const result = res.data.dictionaries.artist_types
                    .map(type => ({
                        ...type, count: res.data.user.artists.filter(a => a.artist_types_id === type.id).length
                    }))
                res.data.dictionaries.artist_types = result
                const managedArtists = res.data.user.managed_artists
                for (const key in managedArtists) {
                    if (managedArtists[key].artist) {
                        res.data.user.artists = [
                            ...res.data.user.artists,
                            {
                                ...managedArtists[key].artist,
                                managed_artist: true
                            }
                        ]
                    }
                }
                // res.data.user.artists = [
                //     ...res.data.user.artists,
                //     ...res.data.user.managed_artists.map(m_a => ({ ...m_a.artist, managed_artist: true }))
                // ]
                return res.data
            })
            .catch(error => {
                switch (error?.response?.status) {
                    case 401:
                        if (router.pathname.includes('artists') || router.pathname === '/') router.push('/login')
                        break;
                    case 403:
                        if (router.pathname !== '/403') router.push('/403')
                        break;
                    case 404:
                        router.push('/404')
                        break;
                    case 409:
                        if (router.pathname.includes('artists') || router.pathname === '/') router.push('/login')
                        setStatus({ code: error.response.status, error: error.response.data.message })

                        break
                    default:
                        break
                }
            })
    )


    const csrf = () => $api.get('/sanctum/csrf-cookie')

    const verifyEmail = async (link, setErrors) => {
        await csrf()

        $api.get(link)
            .then(res => res.data)
            .catch(error => {
                switch (error?.response?.status) {
                    case 401:
                        setErrors({ email: appErros['ru'][error.response.message], error: 401 })
                        break;
                    case 403:
                        setErrors({ email: 'Устаревшая ссылка или неправильная ссылка', status: 409 })
                        break;
                    default:
                        setErrors({ email: 'Что-то пошло не так', status: 500 })
                        break;
                }
            })
    }

    const register = async ({ setErrors, ...props }) => {
        await csrf()

        setErrors([])

        $api.post('/register', props)
            .then(data => router.push('/login'))
            .catch(error => {
                if (error.response.status !== 422) throw error
                const errors = error.response.data.errors
                for (const key in errors) {
                    setErrors(prev => ({ ...prev, [key]: appErros['ru'][errors[key]] }))
                }
            })
    }

    const login = async ({ setErrors, setStatus, ...props }) => {
        await csrf()

        // setErrors([])
        // setStatus(null)

        await $api.post('/login', props)
            .catch(error => {
                switch (error.response.status) {
                    case 422:
                        // setErrors(() => error.response.data.errors)
                        setErrors({ email: appErros['ru'][error.response.data.message] })
                        break
                    case 403:
                        // setErrors(['Доступ запрещен', 'Ваш аккаунт заблокирован'])
                        router.push('/403')
                        break
                    case 409:
                        setErrors({ 'email': appErros['ru'][error.response.data.message], 'status': 409 })
                        break
                    default:
                        setErrors({ 'email': '' })
                        throw error
                }
            })
        mutate()
    }

    const forgotPassword = async ({ setErrors, setStatus, email }) => {
        await csrf()

        setErrors([])
        setStatus(null)

        $api
            .post('/forgot-password', { email })
            .then(response => setStatus(response.data.status))
            .catch(error => {
                if (error.response.status !== 422) throw error

                setErrors(Object.values(error.response.data.errors).flat())
            })
    }

    const resetPassword = async ({ setErrors, setStatus, ...props }) => {
        await csrf()

        setErrors([])
        setStatus(null)

        $api
            .post('/reset-password', { token: router.query.token, ...props })
            .then(response => router.push('/login?reset=' + btoa(response.data.status)))
            .catch(error => {
                if (error.response.status != 422) throw error

                setErrors(Object.values(error.response.data.errors).flat())
            })
    }

    const resendEmailVerification = ({ setStatus }) => {
        $api.post('/email/verification-notification').then(response => setStatus(response.data.status))
    }

    const logout = async () => {
        if (!error) {
            await $api.post('/logout')
            mutate()
            // router.push('/login')
        }
    }

    useEffect(() => {
        if (middleware === 'guest' && redirectIfAuthenticated && app) router.push(redirectIfAuthenticated)
        if (middleware === 'auth' && error) logout()
    }, [app, error])

    return {
        // user,
        mutate,
        app,
        register,
        login,
        forgotPassword,
        resetPassword,
        resendEmailVerification,
        logout,
        error,
        status,
        verifyEmail,
        verifyError
        // res,
        // _error
    }
}
