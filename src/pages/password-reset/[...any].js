import Image from "next/image"
import { useRouter } from "next/router"
import { useEffect, useState } from "react"

import { $api } from 'api'
import { appErros } from 'utils'

export default function PasswordReset() {

    const router = useRouter()
    const { query } = router

    const [inputValues, setInputValues] = useState({
        password: {
            value: '',
            error: false
        },
        confirmation_password: {
            value: '',
            error: false
        }
    })
    const [message, setMessage] = useState(null)
    const [status, setStatus] = useState(null)


    const setInputValue = (field, validationField) => e => setInputValues(prev => ({
        ...prev, [field]: {
            ...prev[field],
            value: e.target.value,
            error: e.target.value.length <= 8
        }
    }))


    const send = async (e) => {
        e.preventDefault()
        if (!inputValues['password']['value'] && !inputValues['confirmation_password']['value']) {
            setInputValues(prev => ({
                ...prev,
                'password': {
                    ...prev['password'], error: true
                },
                'confirmation_password': {
                    ...prev['confirmation_password'], error: true
                }
            }))
            setMessage('Заполните поля')
            return
        }
        if (inputValues['password']['error'] || inputValues['confirmation_password']['error']) {
            setInputValues(prev => ({
                ...prev,
                'password': {
                    ...prev['password'], error: inputValues['password']['error']
                },
                'confirmation_password': {
                    ...prev['confirmation_password'], error: inputValues['confirmation_password']['error']
                }
            }))
            setMessage('Длина пароля не должна быть меньше 8')
            return
        }
        if (inputValues['password']['value'] !== inputValues['confirmation_password']['value']) {
            setMessage('Пароли не совпадают')
            return
        }
        const obj = {
            token: query.any[0],
            email: query.email,
            password: inputValues['password']['value'],
            password_confirmation: inputValues['confirmation_password']['value']
        }
        const { status, data } = await $api.post(`/reset-password`, obj).catch(err => err.response)
        if (status === 200) {
            setStatus(200)
            // setMessage('')
            router.push('/login')
            return
        }

        setStatus(status)
        const key = Object.keys(data.errors)[0]
        setMessage(appErros['ru'][data.errors[key]])
    }

    const sendResetLink = async () => {
        const obj = { email: query.email }
        const { data, status } = await $api.post('/forgot-password', obj).catch(err => err.response)
        if (status === 200) {
            setMessage('Ссылка отправлена вам на почту')
            setStatus(200)
            setTimeout(() => {
                router.push('/login')
            }, 1000);
            return
        }
        setMessage('Отправка не возмона, подождите некоторое время')
    }


    const changePage = () => {
        router.push('/login')
    }

    return (
        <div className="registration">
            <div className="registration__inner">
                <div className="registration__modal">
                    <div className="registration__modal-logo">
                        <Image
                            src={ '/logo.svg' }
                            layout='fill'
                            alt="logo"
                            priority
                        />
                    </div>
                    <form onSubmit={ send }>
                        <div className="registration__modal-top">
                            <div className="registration__modal-tooltip">
                                <div className="registration__modal-tooltip-logo">
                                    <Image
                                        src={ '/user.svg' }
                                        width='100%'
                                        height={ '100%' }
                                        alt='user'
                                    />
                                </div>
                                <span>
                                    Сброс пароля
                                </span>
                            </div>
                        </div>
                        <div className="registration__modal-body">
                            <span className="error">
                                { message }
                                { status === 422 && <span onClick={ sendResetLink } className="submit-email">Отправить повторно</span> }
                                {/* { inputValues['password']['error'] ?? 'Некоректный email' } */ }
                            </span>
                            <div className="registration__body-top">
                                <span>
                                    Пароль
                                </span>
                            </div>
                            <div className={ `registration__body-input login ${inputValues['password']['error'] && 'input__error'}` }>
                                <input
                                    value={ inputValues['password']['value'] }
                                    onChange={ setInputValue('password', 'confirmation_password') }
                                    type="text"
                                    maxLength={ 255 }
                                />
                                <div className="registration__input-icon">
                                    <Image
                                        src={ '/lock.svg' }
                                        width='100%'
                                        height={ '100%' }
                                        alt='email'
                                    />
                                </div>
                            </div>
                            <span className="error">
                                {/* { inputValues['confirmation_password']['error'] ?? 'Пароль не совпадает' } */ }
                            </span>
                            <div className={ `registration__body-top password` }>
                                <span>
                                    Повторите пароль
                                </span>
                            </div>
                            <div className={ `registration__body-input password ${inputValues['confirmation_password']['error'] && 'input__error'}` }>
                                <input
                                    name={ 'password' }
                                    value={ inputValues['confirmation_password']['value'] }
                                    onChange={ setInputValue('confirmation_password', 'password') }
                                    type="password"
                                    maxLength={ 255 }
                                />
                                <div className="registration__input-icon">
                                    <Image
                                        src={ '/lock.svg' }
                                        width='100%'
                                        height={ '100%' }
                                        alt='email'
                                    />
                                </div>
                            </div>
                            <div className="registration__body-sign In">
                                <button onClick={ send }>Отправить</button>
                            </div>
                            <div className="registration__body-sign Up">
                                <span onClick={ changePage }>У вас уже есть аккаунта?</span>
                                {/* <span onClick={ signUp }>Регистрация!</span> */ }
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    )
}