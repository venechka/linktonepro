import { $api } from "api"
import { useAuth } from "hooks/useAuth"
import { useRouter } from "next/router"
import { useEffect } from "react"

export default function VerifyEmail() {

    const router = useRouter()

    useEffect(() => {
        async function getData() {
            if (!router.isReady) return
            router.push({
                pathname: '/login',
                query: {
                    emailVerificationLink: `/verify-email/${router.query.params[0]}/${router.query.params[1]}?expires=${router.query.expires}&signature=${router.query.signature}`
                }
            }, '/login')
        }
        getData()
    }, [router])


    return (
        <div></div>
    )
}