import '../scss/index.scss'
import React, { createContext, useState } from 'react'
import { useAuth } from 'hooks/useAuth'
import { ContentLoader } from 'components/ContentLoader'
import { useRouter } from 'next/router'
import moment from 'moment'

import 'moment/locale/ru'
moment.locale('ru')


export const AppContext = createContext({})

function App({ Component, pageProps }) {
    const { app, mutate } = useAuth({ middleware: 'auth', })
    const router = useRouter()

    const isHas = router.asPath.match(/^\/(login|sign-up|recover-password|403|password-reset|verify-email)/g)
    if (!app && !isHas) return <ContentLoader />

    return <AppContext.Provider value={ app ? { app, mutate } : {} }>
        <Component { ...pageProps } />
    </AppContext.Provider>

}
export default App

