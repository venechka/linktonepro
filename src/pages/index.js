import { Card } from "components/Card";
import { ContentFilter } from "components/ContentFilter";
import { ContentLoader } from "components/ContentLoader";
import { CommonLayout } from "layouts/Common";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useContext, useEffect, useState } from "react";
import { Button } from "ui/Button";
import { SearchInput } from "ui/Search";
import { AppContext } from "./_app";


export default function Main() {

    const router = useRouter()

    const [searchValue, setSearchValue] = useState('')
    const setSearchInputValue = e => setSearchValue(e.target.value)

    const { app } = useContext(AppContext)

    const [selectedType, setSelectedType] = useState(0)

    if (!app) return <ContentLoader />


    const addArtist = () => router.push(`/artists/create`)

    const filterArtistsByType = (type) => {
        if (app.dictionaries.artist_types) {
            if (type === null) {
                setSelectedType(0)
                return
            }
            setSelectedType(type)
        }
    }
    const filterByName = item => item && String(item?.name?.toLowerCase()).includes(searchValue.toLowerCase())

    const sortByDate = (a, b) => {
        if (new Date(a.created_at).setSeconds(0, 0) < new Date(b.created_at).setSeconds(0, 0)) {
            return 1
        }
        return -1
    }

    return (
        <CommonLayout
            contentHeaderTitle={ 'Артисты' }
            title='Артисты'
        >
            <div className="artists__content">
                <div className="mainInfo">
                    <div className="mainInfo__inner">
                        <div className="mainInfo__top">
                            <div className="mainInfo__top-left">
                                <Button
                                    color="primary"
                                    txt={ `+ Добавить артиста` }
                                    onClick={ addArtist }
                                />
                            </div>
                            <div className="mainInfo__top-right">
                                <SearchInput
                                    type="solid"
                                    value={ searchValue }
                                    onChange={ setSearchInputValue }
                                />
                            </div>
                        </div>
                        <div className="mainInfo__content">
                            <div className="mainInfo__content-left">
                                <ContentFilter
                                    title={ 'Типы артистов' }
                                    bodyItems={ app.dictionaries.artist_types.filter(t => t.count !== 0) ?? [] }
                                    setArr={ filterArtistsByType }
                                    esence={ app.user.artists }
                                />
                            </div>
                            <div className="mainInfo__content-right">
                                {
                                    !app.user.artists.length
                                        ? <h1 className="empty">У вас нет артистов</h1>
                                        : <ul className="mainInfo__content-cards">
                                            {
                                                app.user.artists
                                                    .filter(filterByName)
                                                    .sort(sortByDate)
                                                    .map(_item =>
                                                        <Link
                                                            key={ _item.id }
                                                            href={ `/artists/${_item?.id}/profile` }
                                                            passHref
                                                        >
                                                            <li
                                                                className={ `mainInfo__content-card 
                                                                ${(selectedType !== _item.artist_types_id && selectedType !== 0) ? 'animated' : ''}`
                                                                }
                                                                data-type={ _item.artist_types_id }
                                                            >
                                                                <Card
                                                                    title={ _item.name }
                                                                    img={ _item.logo }
                                                                    type={ app.dictionaries.artist_types.find(t => t.id === _item.artist_types_id).name }
                                                                />
                                                            </li>
                                                        </Link>
                                                    )
                                            }
                                        </ul>
                                }

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </CommonLayout>
    )
}