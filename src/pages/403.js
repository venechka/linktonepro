import { useRouter } from "next/router"

export default function Forbidden() {

    const router = useRouter()

    return <div
        className="banned"
        style={ {
            height: '100vh',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center'
        } }
    >
        <div className="banned__inner">
            <h3 style={
                {
                    fontSize: 32,
                    color: '#08C',
                    marginBottom: 30,
                }
            }>
                Вы были забанены
            </h3>
            <span
                onClick={ () => router.push('/login') }
                style={
                    {
                        fontSize:18,
                        cursor:'pointer',
                        padding:'10px 22px',
                        border:'1px solid #0088CC',
                        borderRadius:10,
                        color:'#0088CC',
                    }
                }
            >
                Войти под другим пользователем
            </span>
        </div>
    </div>
}