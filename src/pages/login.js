import { SignIn } from "components/SignIn"
import Image from "next/image"
import React, { useState } from "react"

export default function Login() {

    
    return (
        <div className="registration">
            <div className="registration__inner">
                <div className="registration__modal">
                    <div className="registration__modal-logo">
                        {/* <img src={ '/logo.svg' } alt="" /> */}
                        <Image
                            src={'/logo.svg'}
                            layout='fill'
                            alt="logo"
                            priority
                        />
                    </div>
                    <SignIn />
                </div>
            </div>
        </div>
    )
}


