import { $api, swrGetFetcher } from "api";
import { VideoFormPage } from "components/VideoFormPage";
import { CommonLayout } from "layouts/Common";
import { useRouter } from "next/router";
import useSWR from "swr";

export default function ExactVideo() {
    return (
        <VideoFormPage update />
    )
}