import { VideoFormPage } from "components/VideoFormPage";
import { CommonLayout } from "layouts/Common";

export default function VideosCreate() {

    return (
        <VideoFormPage />
    )
}