import { $api, swrGetFetcher } from "api";
import { Card } from "components/Card";
import { ContentFilter } from "components/ContentFilter";
import { ContentLoader } from "components/ContentLoader";
import { CommonLayout } from "layouts/Common";
import Link from "next/link";
import { useRouter } from "next/router";
import { AppContext } from "pages/_app";
import { useContext, useEffect, useState } from "react";
import { __SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED } from "react-dom";
import useSWR from "swr";
import { Button } from "ui/Button";
import { SearchInput } from "ui/Search";
import { getVideoImg } from "utils";




export default function Videos() {

    const router = useRouter()
    const { id } = router.query


    const { app } = useContext(AppContext)

    const { data: artist } = useSWR(app ? `/api/artists/${id}` : null, swrGetFetcher)

    const [dictionaries, setDictionaries] = useState(null)
    useEffect(() => {
        if (!artist) return
        setDictionaries(app.dictionaries.video_types.map(type => ({ ...type, count: artist.videos.filter(v => v.video_type_id === type.id).length })))
    }, [artist,app.dictionaries.video_types])

    const [searchValue, setSearchValue] = useState('')
    const setSearchInputValue = e => setSearchValue(e.target.value)

    const filterByName = (item) => String(item.name.toLowerCase()).includes(searchValue.toLowerCase())

    const [selectedType, setSelectedType] = useState(0)
    const filterVideosByType = type => {
        if (artist) {
            if (type === null) {
                setSelectedType(0)
                return
            }
            setSelectedType(type)
        }
    }

    const addVideo = () => router.push(`/artists/${id}/videos/create`)

    const sortByDate = (a, b) => {
        if (new Date(a.created_at).setSeconds(0, 0) < new Date(b.created_at).setSeconds(0, 0)) {
            return 1
        }
        return -1
    }

    if (!artist) return <ContentLoader />

    return (
        <CommonLayout
            contentHeaderTitle={ 'Видео' }
            crumbs={ [
                { id: 0, title: artist ? artist.name : 'Артист' }
            ] }
            isUpdate={ { link: `${process.env.NEXT_PUBLIC_APP_URL}/${artist?.slug}/videos` } }
            title={`Видео артиста ${artist.slug}`}
        >
            <div className="mainInfo">
                <div className="mainInfo__inner">
                    <div className="mainInfo__top">
                        <div className="mainInfo__top-left">
                            <Button
                                color="primary"
                                txt={ `+ Добавить видео` }
                                onClick={ addVideo }
                            />
                        </div>
                        <div className="mainInfo__top-right">
                            <SearchInput
                                type="solid"
                                value={ searchValue }
                                onChange={ setSearchInputValue }
                            />
                        </div>
                    </div>
                    <div className="mainInfo__content">
                        <div className="mainInfo__content-left">
                            <ContentFilter
                                title={ 'Типы видео' }
                                bodyItems={ dictionaries ? dictionaries.filter(d => d.count !== 0) : [] }
                                setArr={ filterVideosByType }
                                esence={ artist?.videos }
                            />
                        </div>
                        <div className="mainInfo__content-right">
                            {
                                !artist.videos.length
                                    ? <h1 className="empty">У вас нет видео</h1>
                                    : <ul className="mainInfo__content-cards">
                                        {
                                            artist.videos
                                                .filter(filterByName)
                                                .sort(sortByDate)
                                                .map(_item =>
                                                    <Link
                                                        key={ _item.id }
                                                        href={ `/artists/${id}/videos/${_item.id}` }
                                                        passHref
                                                    >
                                                        <li
                                                            className={ `mainInfo__content-card  
                                                                        ${(selectedType !== 0 && selectedType !== _item.video_type_id) && 'animated'}`
                                                            }
                                                        >
                                                            <Card
                                                                title={ _item.name }
                                                                type={ dictionaries && dictionaries.find(t => t.id === _item.video_type_id).name }
                                                                img={ getVideoImg(_item.link) }
                                                                format='video'
                                                            />
                                                        </li>
                                                    </Link>
                                                )
                                        }
                                    </ul>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </CommonLayout >
    )
}