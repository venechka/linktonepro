import { $api, swrGetFetcher } from "api";
import { Card } from "components/Card";
import { ContentFilter } from "components/ContentFilter";
import { ContentLoader } from "components/ContentLoader";
import { CommonLayout } from "layouts/Common";
import Link from "next/link";
import { useRouter } from "next/router";
import { AppContext } from "pages/_app";
import { useContext, useEffect, useState } from "react";
import useSWR from "swr";
import { Button } from "ui/Button";
import { SearchInput } from "ui/Search";





export default function Relizes() {


    const router = useRouter()
    const { id } = router.query

    const { app } = useContext(AppContext)

    const [searchValue, setSearchValue] = useState('')
    const setSearchInputValue = e => setSearchValue(e.target.value)

    const { data: artist } = useSWR(app ? `/api/artists/${id}` : null, swrGetFetcher)

    const [dictionaries, setDictionaries] = useState(null)

    useEffect(() => {
        if (!artist) return
        setDictionaries(app.dictionaries.release_types.map(type => ({ ...type, count: artist.releases.filter(r => r.release_type_id === type.id).length })))
    }, [artist,app.dictionaries.release_types])


    const filterByName = (item) => String(item.name.toLowerCase()).includes(searchValue.toLowerCase())

    const [selectedType, setSelectedType] = useState(0)
    const filterReleaseByType = (type) => {
        if (artist.releases) {
            if (type === null) {
                setSelectedType(0)
                return
            }
            setSelectedType(type)
        }
    }

    const addRelease = () => router.push(`/artists/${id}/releases/create`)

    const sortByDate = (a, b) => {
        if (new Date(a.created_at).setSeconds(0, 0) < new Date(b.created_at).setSeconds(0, 0)) {
            return 1
        }
        return -1
    }

    if (!artist) return <ContentLoader />

    return (
        <CommonLayout
            contentHeaderTitle={ 'Релизы' }
            crumbs={ [
                { id: 0, title: artist?.name ?? '' }
            ] }
            isUpdate={ { link: `${process.env.NEXT_PUBLIC_APP_URL}/${artist?.slug}/releases` } }
            title={`Релизы артиста ${artist.slug}`}
        >
            <div className="mainInfo">
                <div className="mainInfo__inner">
                    <div className="mainInfo__top">
                        <div className="mainInfo__top-left">
                            <Button
                                color="primary"
                                txt={ `+ Добавить релиз` }
                                onClick={ addRelease }
                            />
                        </div>
                        <div className="mainInfo__top-right">
                            <SearchInput
                                type="solid"
                                value={ searchValue }
                                onChange={ setSearchInputValue }
                            // placeholder={ searchPlaceholder }
                            />
                        </div>
                    </div>
                    <div className="mainInfo__content">
                        <div className="mainInfo__content-left">
                            <ContentFilter
                                title={ 'Типы релизов' }
                                bodyItems={ dictionaries ? dictionaries.filter(d=>d.count !== 0) : [] }
                                setArr={ filterReleaseByType }
                                esence={ artist.releases }
                            />
                        </div>
                        <div className="mainInfo__content-right">
                            {
                                !artist.releases.length
                                    ? <h1 className="empty">У вас нет релизов</h1>
                                    : <ul className="mainInfo__content-cards">
                                        {
                                            artist.releases
                                                .filter(filterByName)
                                                .sort(sortByDate)
                                                .map(_item =>
                                                    <Link
                                                        key={ _item.id }
                                                        href={ `/artists/${artist?.id}/releases/${_item.id}` }
                                                        passHref
                                                        >
                                                        <li
                                                            className={ `mainInfo__content-card 
                                                                        ${(selectedType !== _item.release_type_id && selectedType !== 0) ? 'animated' : ''}`
                                                            }
                                                        >
                                                            <Card
                                                                title={ _item.name }
                                                                img={ _item.cover }
                                                                type={ dictionaries &&  dictionaries.find(t => t.id === _item.release_type_id).name }
                                                            />
                                                        </li>
                                                    </Link>
                                                )
                                        }
                                    </ul>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </CommonLayout>
    )
}