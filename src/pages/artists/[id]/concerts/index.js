import { swrGetFetcher } from "api";
import { ContentLoader } from "components/ContentLoader";
import { CommonLayout } from "layouts/Common";
import moment from "moment";
import Link from "next/link";
import { useRouter } from "next/router";
import { AppContext } from "pages/_app";
import { useContext, useEffect, useState } from "react";
import useSWR from "swr";
import { Button } from "ui/Button";
import { SearchInput } from "ui/Search";




export default function Concerts() {

    const router = useRouter()
    const { id } = router.query

    const { app } = useContext(AppContext)
    const { data: artist } = useSWR(id ? `/api/artists/${id}` : null, swrGetFetcher)

    const [searchValue, setSearchValue] = useState('')
    const [concertsArr, setConcertsArr] = useState([])

    useEffect(() => {
        setConcertsArr(artist?.concerts.sort((a, b) => new Date(b.start_datetime) - new Date(a.start_datetime)))
    }, [artist])

    if (!app) return <ContentLoader />

    const createConcert = () => router.push(`/artists/${id}/concerts/create`)
    const setInputsValue = (field) => e => setSearchValue(e.target.value)

    const filterByName = item => String(item.city.toLowerCase()).includes(searchValue.toLowerCase())

    const filterByDate = item => {
        const value = item.target.value
        switch (value) {
            case '0':
                setConcertsArr(artist.concerts.filter(c => new Date(c.start_datetime).getTime() > new Date().getTime()))
                break;
            case '1':
                setConcertsArr(artist.concerts.filter(c => new Date(c.start_datetime).getTime() < new Date().getTime()))
                break;
            default:
                setConcertsArr(artist.concerts)
                break;
        }
    }

    const parseDate = date => {
        const d = date.split(' ')[0]
        const t = date.split(' ')[1]

        const newD = new Date(`${d}T${t}`)
            .toLocaleString('ru-Ru',
                {
                    day: 'numeric', month: 'numeric', year: 'numeric'
                }
            )
        return newD
    }

    return (
        <CommonLayout
            contentHeaderTitle={ 'Концерты' }
            crumbs={ [
                { id: 1, title: artist ? artist.name : 'Артист' },
            ] }
            isUpdate={ { link: `${process.env.NEXT_PUBLIC_APP_URL}/${artist?.slug}/concerts` } }
            title={ `Концерты артиста ${artist?.slug}` }
        >
            <div className="concerts">
                <div className="concerts__inner">
                    {/* {
                        !isLoading
                            ? <> */}
                    <div className="concerts__table">
                        <div className="concerts__table-top">
                            <div className="concerts__top-left">
                                <Button
                                    color='primary'
                                    txt='+ Добавить концерт'
                                    onClick={ createConcert }
                                />
                            </div>
                            <div className="concerts__top-right">
                                <div className="concerts__right-status">
                                    <span>Статус:</span>
                                    <select onChange={ filterByDate }>
                                        <option value="0">Предстоящие</option>
                                        <option value="1">Прошедшие</option>
                                        <option value="2">Все</option>
                                    </select>
                                </div>
                                <div className="concerts__right-input">
                                    <SearchInput
                                        type="solid"
                                        placeholder="Найти концерт"
                                        value={ searchValue }
                                        onChange={ setInputsValue('searchValue') }
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="concerts__table-content">
                            <ul className="concerts__content-items">
                                <li className="concerts__content-item top">
                                    <span className="concerts__item">Дата</span>
                                    <span className="concerts__item">Город</span>
                                    <span>Площадка</span>
                                    <span>Тип выступления</span>
                                </li>
                                {
                                    concertsArr && concertsArr
                                        // artist && artist.concerts
                                        .filter(filterByName)
                                        .map(_item =>
                                            <div key={ _item.id } className="concerts__content-item-wrapper">
                                                <Link href={ `/artists/${id}/concerts/${_item.id}` } passHref>
                                                    <li className="concerts__content-item">
                                                        <span className="concerts__item-date">
                                                            {
                                                                moment(_item?.start_datetime).format('L')
                                                            }
                                                        </span>
                                                        <span className="concerts__item-city">{ _item.city }</span>
                                                        <span>{ _item.venue }</span>
                                                        <span>
                                                            { app.dictionaries.concert_types &&
                                                                app.dictionaries.concert_types
                                                                    ?.find(type => type.id === _item.concert_type_id)?.name
                                                            }
                                                        </span>
                                                    </li>
                                                </Link>
                                            </div>
                                        )
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </CommonLayout>
    )
}