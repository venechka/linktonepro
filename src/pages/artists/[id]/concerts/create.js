import { ConcertFormPage } from "components/ConcertFormPage";
import { CommonLayout } from "layouts/Common";

export default function ConcertsCreate() {
    return <ConcertFormPage />
}