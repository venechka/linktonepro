import { $api, swrGetFetcher } from "api";
import { ContentLoader } from "components/ContentLoader";
import { CurrentPageMain } from "components/CurrentPageMain";
import { useLoading } from "hooks/useLoading";
import { CommonLayout } from "layouts/Common";
import Image from "next/image";
import { useRouter } from "next/router";
import { AppContext } from "pages/_app";
import { useContext, useEffect, useState } from "react";
import useSWR from "swr";
import { Button } from "ui/Button";
import { Input } from "ui/Input";
import { SearchInput } from "ui/Search";



export default function Settings() {

    const router = useRouter()
    const { id } = router.query

    const { data: artist, mutate } = useSWR(id ? `/api/artists/${id}` : null, swrGetFetcher)

    const { app } = useContext(AppContext)

    const [owner, setOwner] = useState({})
    const [managers, setManagers] = useState([])
    const [condidate, setCondidate] = useState(null)
    const [candidates, setCandidates] = useState([])
    const [selected, setSelected] = useState([])
    const [findUserError, setFindUserError] = useState('Нет найденых пользователей')
    const [managersMessage, setManagersMessage] = useState('Нет выбранных администраторов')

    const { isLoading } = useLoading(artist)

    const [searchValue, setSearchValue] = useState('')
    const setSearchInputValue = e => setSearchValue(e.target.value)

    useEffect(() => {
        setOwner({ ...artist?.owner } ?? {})
        setManagers(artist?.managers ?? [])
        // setSelected(managers ? managers.map(item => item.id) : [])
        return () => {
            setCandidates([])
            setManagers([])
            setOwner({})
            setSearchValue('')
        }
    }, [artist])


    const regexp = /[\w\d]+@[\d\w]+/g
    const findManager = async () => {
        if (regexp.test(searchValue.toLowerCase())) {
            try {
                const { data: user } = await $api.post('/api/app/getUserByEmail', { email: searchValue })
                if (!user) {
                    setFindUserError('Пользователь не найден')
                    return
                }
                if (user.id === app.user.id) {
                    setFindUserError('Вы не можете добавить себя')
                    return
                }
                setCondidate(user)
            } catch (error) {
                switch (error.response.status) {
                    case 404:
                        setFindUserError('Такой email не существует')
                        break;
                    case 422:
                        setFindUserError('Неверно введен email')
                        break;
                    default:
                        break;
                }
            }
        }
    }

    const addManager = managerId => async () => {
        const detected = managers.find(m => m.user.id === managerId)
        if (!detected && managerId !== app.user.id) {
            await $api.post(`/api/artists/${id}`, {
                _method: 'PUT',
                managers: JSON.stringify([...managers.map(item => item.user.id), managerId])
            })
            mutate()
            return
        }
    }


    const removeManager = managerId => async () => {

        setManagers(prev => [...prev.filter(item => item.id !== managerId)])

        await $api.post(`/api/artists/${id}`, {
            _method: 'PUT',
            managers: JSON.stringify(managers.filter(item => item.id !== managerId).map(item => item.user.id))
        })
        mutate()
    }


    const cancel = () => router.push(`/`)


    return <CommonLayout
        contentHeaderTitle={ 'Настройки артиста' }
        crumbs={ [
            { id: 1, title: artist ? artist.name : 'Артист' }
        ] }
        title={`Настройки артиста ${artist?.slug}`}
    >
        <div className="currentInfo">
            <div className="currentInfo__wrapper">
                {
                    !isLoading
                        ?
                        <div className="currentInfo__inner">
                            <div className="currentInfo__main">
                                <div className="currentInfo__main-left">
                                    <div className="currentInfo__left-icon">
                                        <Image src={ '/box.svg' } width='83px' height='83px' alt="box" />
                                    </div>
                                    <h2 className="currentInfo__left-title">Добавление администратора</h2>
                                    <p className="currentInfo__left-text">Здесь вы можете добавить пользователя к управлению профилем артиста</p>
                                </div>
                                <div className="currentInfo__main-right">
                                    <div className="settings__wrapper">
                                        <div className="settings__top">
                                            <span>Поиск администратора</span>
                                            <div className="settings__top-search">
                                                <SearchInput
                                                    placeholder="Введите email"
                                                    value={ searchValue }
                                                    onChange={ setSearchInputValue }
                                                    onClick={ findManager }
                                                />
                                            </div>
                                        </div>
                                        <div className="settings__bottom">
                                            <span className="settings__bottom-title">Результат поиска</span>
                                            <div className="settings__bottom-body">
                                                {
                                                    condidate ?
                                                        <div
                                                            className="settings__body-item">
                                                            <div className="settings__item-wrapper">
                                                                <div className="owner__img">
                                                                    {/* <img src={ condidate.picture } alt="" /> */}
                                                                    <Image
                                                                        loader={ () => condidate.picture ?? '/user.svg' }
                                                                        src={ condidate.picture ?? '/user.svg' }
                                                                        layout='fill'
                                                                        alt="box"
                                                                        unoptimized
                                                                    />
                                                                </div>
                                                                <div>
                                                                    <span className="owner__name">{ condidate.name }</span>
                                                                    <span className="owner__email">{ condidate.email }</span>
                                                                </div>
                                                            </div>
                                                            <div onClick={ addManager(condidate.id) } className="owner__add">
                                                                +
                                                            </div>
                                                        </div>
                                                        : <div>{ findUserError }</div>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="currentInfo__managers">
                                <div className="currentInfo__main-left">
                                    <div className="currentInfo__left-icon">
                                        {/* <Image src={ '/profile.svg' } width='83px' height='83px' /> */ }
                                    </div>
                                    <h2 className="currentInfo__left-title">Участники</h2>
                                    <p className="currentInfo__left-text">Здесь вы можете видеть администраторов артиста</p>
                                </div>
                                <div className="currentInfo__main-right">
                                    <div className="settings__right">
                                        <span className="settings__bottom-title">Создатель</span>
                                        {
                                            owner
                                                ? <div className="settings__body-owner">
                                                    <div className="owner__img">
                                                        {/* <img src={ owner.picture ?? '/user.svg' } alt="" /> */}
                                                        <Image
                                                            layout="fill"
                                                            src={ owner.picture ?? '/user.svg' }
                                                            loader={ () => owner.picture ?? '/user.svg' }
                                                            alt='img'
                                                            unoptimized
                                                        />
                                                    </div>
                                                    <div>
                                                        <span className="owner__name">{ owner.name }</span>
                                                        <span className="owner__email">{ owner.email }</span>
                                                    </div>
                                                </div>
                                                : <div>...loading</div>
                                        }
                                    </div>
                                    <div className="settings__left">
                                        <span className="settings__bottom-title">Администраторы</span>
                                        <div className="settings__body-managers">
                                            {
                                                managers.length
                                                    ? managers.map(manager =>
                                                        <div key={ manager.id } className="settings__body-item">
                                                            <div className="settings__item-wrapper">
                                                                <div className="owner__img">
                                                                    {/* <img src={ manager?.user?.picture } alt="" /> */}
                                                                    <Image
                                                                        layout="fill"
                                                                        src={ manager.user.picture ?? '/user.svg' }
                                                                        loader={ () => manager.user.pictures ?? 'user.svg' }
                                                                        alt='img'
                                                                        unoptimized
                                                                    />
                                                                </div>
                                                                <div>
                                                                    <span className="owner__name">{ manager?.user?.name }</span>
                                                                    <span className="owner__email">{ manager?.user?.email }</span>
                                                                </div>
                                                            </div>
                                                            <div onClick={ removeManager(manager.id) } className="owner__remove">
                                                                -
                                                            </div>
                                                        </div>
                                                    )
                                                    : <div>{ managersMessage }</div>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="currentInfo__btns">
                                <div className="currentInfo__btns-left">
                                    {/* <Button onClick={ saveArtist() } txt='Сохранить' color='primary' /> */ }
                                    <Button onClick={ cancel } txt='Вернуться' color='cancel' />
                                </div>
                            </div>
                        </div>
                        : <ContentLoader />
                }
            </div>
        </div>


    </CommonLayout>
}