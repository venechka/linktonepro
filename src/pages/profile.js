import { $api, swrGetFetcher } from "api";
import { CurrentPageDropZone } from "components/CurrentPageDropZone";
import { CurrentPageMain } from "components/CurrentPageMain";
import { CommonLayout } from "layouts/Common";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "ui/Button";
import Form from "components/Form";
import { useFormik } from 'formik';
import * as Yup from 'yup'
import { useLoading } from "hooks/useLoading";
import { ContentLoader } from "components/ContentLoader";
import { AppContext } from "./_app";
import NotValidContent from "components/NotValidContent";
import { appErros, MainFormContext } from "utils";
import { useRequest } from "hooks/useRequest";



export default function Profile() {

    const [imgAvatar, setImgAvatar] = useState('')

    // const dispatch = useDispatch()
    // const { user } = useSelector(state => state.user)

    const { app, mutate } = useContext(AppContext)

    const { isLoading } = useLoading(app)

    const router = useRouter()

    const [defaultValues, setDefaultValues] = useState(
        { name: '', lastname: '', email: '', tel: '', country: '', city: '' }
    )

    const { set } = useRequest()


    const [unProcessableContent, setUnProcessableContent] = useState(null)

    useEffect(() => {

        if (app) {
            const {
                name,
                lastname,
                email,
                phone,
                country,
                city
            } = app.user

            const validatedData = {
                name,
                lastname,
                email,
                tel: phone ?? '',
                country: country ?? '',
                city: city ?? ''
            }
            setDefaultValues(validatedData)
        }
    }, [app])// eslint-disable-line react-hooks/exhaustive-deps


    const [inputsValues, setInputsValues] = useState(null)
    const saveUser = async data => {
        const { errors, ...other } = inputsValues

        for (const key in other) {
            if (!other[key]) {
                setUnProcessableContent('Заполните все поля')
                return
            }
        }
        const formData = new FormData()

        formData.append('name', other['name'])
        formData.append('lastname', other['lastname'])
        formData.append('email', other['email'])
        formData.append('phone', other['tel'])
        formData.append('country', other['country'])
        formData.append('city', other['city'])

        if (imgAvatar) formData.append('picture', imgAvatar)
        // formData.append('_method', 'PUT')

        // await $api.post(, formData)
        const error = await set(`/api/app/user`, formData, `/`)
        if (error) setUnProcessableContent(appErros['ru'][error])
        mutate()
    }

    const cancel = () => router.push(`/`)


    const [inputsErrors, setInputsErrors] = useState({
        cover: false
    })


    const schema = Yup.object().shape({
        name: Yup.string()
            .required('Введите имя')
            .label('Имя'),
        lastname: Yup.string()
            .label('Фамилия'),
        email: Yup.string()
            .required('Введите email')
            .label('E-mail'),
        tel: Yup.string()
            .label('Телефон'),
        country: Yup.string()
            .required('Введите страну')
            .meta({
                country: true,
                route: '/api/app/searchCountry',
                type: 'country'
            })
            .label('Страна'),
        city: Yup.string()
            .required('Введите город')
            .meta({
                city: true,
                route: '/api/app/searchCity',
                type: 'city',
                // country:
            })
            .label('Город'),
    })

    return <CommonLayout
        contentHeaderTitle={ 'Настройки профиля' }
        title={`Настройки профиля`}
    >
        {
            unProcessableContent && <NotValidContent error={ unProcessableContent } />
        }
        <div className="currentInfo">
            <div className="currentInfo__wrapper">
                {
                    !isLoading
                        ? <>
                            <div className="currentInfo__inner">
                                <MainFormContext.Provider value={ { setInputsValues } }>
                                    <CurrentPageMain
                                        esenceTitle={ 'Внесите данные о себе и загрузите аватар' }
                                        defaultValues={ defaultValues }
                                        schema={ schema }
                                    />
                                </MainFormContext.Provider>
                                {/* <Form
                                        formik={ formik }
                                        schema={ schema }
                                    />
                                </CurrentPageMain> */}
                                <CurrentPageDropZone
                                    title={ 'Аватар' }
                                    text={ 'Загрузите аватар профиля' }
                                    setImg={ setImgAvatar }
                                    img={ app ? app.user.picture : '' }
                                    setError={ setInputsErrors }
                                    error={ inputsErrors['cover'] }
                                    setMessage={ setUnProcessableContent }
                                />
                                <div className="currentInfo__btns">
                                    <div className="currentInfo__btns-left">
                                        {/* <Button onClick={ formik.handleSubmit } txt='Сохранить' color='primary' /> */ }
                                        <Button onClick={ saveUser } txt='Сохранить' color='primary' />
                                        <Button onClick={ cancel } txt='Отменить' color='cancel' />
                                    </div>
                                </div>
                            </div>
                        </>
                        : <ContentLoader />
                }
            </div>
        </div>
    </CommonLayout>
}