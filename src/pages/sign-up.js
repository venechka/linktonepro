import { SignUp } from "components/SignUp";
import Image from "next/image";
import { useState } from "react";


export default function signUp() {

    return <div className="registration">
        <div className="registration__inner">
            <div className="registration__modal">
                <div className="registration__modal-logo">
                    {/* <img src={ '/logo.png' } alt="" /> */ }
                    <Image
                        src={ '/logo.svg' }
                        layout='fill'
                        alt="logo"
                        priority
                    />
                </div>
                <SignUp />
            </div>
        </div>
    </div>

}