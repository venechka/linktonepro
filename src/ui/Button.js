
export function Button({ color = 'default', txt, onClick }) {
    return (<button  onClick={ onClick } className={ `btn ${color}` }>{ txt }</button>)
}