import { Autocomplete, TextField } from "@mui/material";

export function Select({ title = 'Группа', dropDownItems = [{ id: 1, title: 'sss' }], onChange, value, error, name }) {

    return <div className="_select">
        <span className="_select__label">{ title }</span>
        <select name={ name } value={ value } onChange={ onChange } className={ `_select__select ${error ? 'input__error' : ''}` }>
            <option value="">- Не выбранно</option>
            {
                dropDownItems && dropDownItems.map(_item =>
                    <option
                        key={ _item.id }
                        value={ _item.id }
                    // selected={ value === _item.id }
                    >{ _item.name }</option>
                )
            }
        </select>
    </div>
    // return <div className="_select">
    //     <div className="_select__top">
    //         <span>{ title }</span>
    //         <img src="" alt="arrowDown" />
    //     </div>
    //     <div className="_select__dropDown">
    //         <ul>
    //             {
    //                 dropDownItems.map(_item =>
    //                     <li></li>
    //                 )
    //             }
    //         </ul>
    //     </div>
    // </div>
}