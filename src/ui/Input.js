import Image from 'next/image'

export function Input(config) {
    const {
        labelTxt,
        type = 'text',
        placeholder = '',
        withIcon = false,
        withFlag = false,
        icon = '/web.svg',
        iconBg = 'web',
        value = '',
        flag = '/russia.png',
        onChange,
        error,
        refLink,
        name,
        isConcert,
        options
    } = config

    return <div className={ `_input ${withIcon ? 'icon' : ''}` }>
        <span className={ `_input__label ${name} ${isConcert ? 'not' : ''}` }>{ labelTxt }</span>
        {
            withIcon ?
                <>
                    <div className={ `_input__item  ${error ? 'input__error' : ''}` }>
                        <div className={ `_input__item-icon ${iconBg}` }>
                            <Image src={ `${icon}` } width='16px' height='16px' />
                        </div>
                        <input
                            name={ name }
                            className={ `_input__item-input` }
                            ref={ refLink }
                            type={ type }
                            value={ value }
                            onChange={ onChange }
                            placeholder={ placeholder }
                            maxLength={ 255 }

                        />
                        {
                            withFlag
                                ? <div className={ `_input__item-flag gray` }>
                                    <Image src={ `${flag}` } width='16px' height='16px' />
                                </div>
                                : <></>
                        }

                    </div>
                </>
                : <input
                    name={ name }
                    className={ `_input__input ${error ? 'input__error' : ''} ` }
                    type={ type }
                    value={ value }
                    onChange={ onChange }
                    placeholder={ placeholder }
                    maxLength={ options?.maxLength ?? 255 }

                />
        }
    </div>
}