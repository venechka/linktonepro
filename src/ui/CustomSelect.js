import { $api } from "api"
import axios from "axios"
import { useEffect, useRef, useState } from "react"
import { useDebouncedCallback } from "use-debounce"

export function CustomSelect(config) {

    const {
        name,
        value,
        setFieldValue,
        label,
        api,
        error
    } = config


    const [dropDownItems, setDropDownItems] = useState([])
    const [isOpened, setIsOpened] = useState(false)

    // const [inputValue, setinputValue] = useState('')
    const [filterInputValue, setFilterInputValue] = useState('')

    const [selectedIndex, setSelectedIndex] = useState(0)


    const regexp = /^[a-zA-Zа-яА-ЯЁ\(\)-\.]{1,}$/

    const debounced = useDebouncedCallback(async (value) => {
        if (!regexp.test(String(value).toLowerCase())) return
        const { data: res } = await $api.post(api.route, api?.country ? { q: value, country: api.country } : { q: value })
        if (Array.isArray(res) && !res.length) {
            setDropDownItems(res)
            return
        }
        setDropDownItems(res)
    }, 500)


    useEffect(() => {
        setIsOpened(dropDownItems.length)
        setSelectedIndex(0)
    }, [dropDownItems])


    const openDropDown = () => {
        if (!dropDownItems || !dropDownItems.length) return
        setIsOpened(!isOpened)
    }

    const setInputValueHandler = title => {
        setFieldValue(name,title)
        if (!title.length) {
            setDropDownItems([])
        }
        else debounced(title)

    }

    const selectValue = item => () => {
        setFieldValue(name, item[api.type])
    }

    const dropDownRef = useRef()
    const dropDownItemRef = useRef()

    const onKeyDownInput = e => {
        if (!isOpened) return
        if (e.key === 'ArrowDown' && selectedIndex !== dropDownItems.length - 1) {
            setSelectedIndex(prev => prev + 1)
            dropDownRef.current.scrollTop = selectedIndex === 1 ? 0 : selectedIndex * 40
            return
        }
        if (e.key === 'ArrowUp' && selectedIndex !== 0) {
            setSelectedIndex(prev => prev - 1)
            dropDownRef.current.scrollTop = selectedIndex === 1 ? 0 : (selectedIndex * 40) - 140
            return
        }
        if (e.key === 'Enter' && selectedIndex !== -1) {
            setFieldValue(name, dropDownItems[selectedIndex].country || dropDownItems[selectedIndex].city)
            setIsOpened(false)
            return
        }
    }

    const [isMouseOver, setIsMouseOver] = useState(false)


    // const hoverAndLeaveMouse = () =>{
    //     setIsMouseOver(!isMouseOver)
    // }

    const onMouseOverOnDropDown = () => {
        setIsMouseOver(true)
    }
    const onMouseLeaveOnDropDown = () => {
        setIsMouseOver(false)
    }

    const onBlurInput = () => {
        if (!isMouseOver) {
            setIsOpened(false)
        }
    }

    return (
        <div className={ `customSelect ${isOpened && 'opened'}` }>
            {/* <div className="customSelect__label"> */ }
            <span className="customSelect__label">{ label }</span>
            {/* </div> */ }
            <div className="customSelect__right" onClick={ openDropDown }>
                <div className={ `customSelect__input ${error && 'input__error'}` }>
                    <input
                        value={ value }
                        onChange={ e => setInputValueHandler(e.target.value) }
                        onKeyUp={ onKeyDownInput }
                        onBlur={ onBlurInput }
                        type="text"
                    />
                </div>
                <div className="customSelect__dropDown" ref={ dropDownRef }>
                    <ul className="customSelect__dropDown-items" >
                        {
                            dropDownItems && dropDownItems
                                .map((item, index) =>
                                    <li
                                        key={ item[api.type] }
                                        className={ `customSelect__dropDown-item ${selectedIndex === index && 'hovered'}` }
                                        value={ item.id }
                                        onClick={ selectValue(item) }
                                        onMouseOver={ onMouseOverOnDropDown }
                                        onMouseLeave={ onMouseLeaveOnDropDown }
                                    >
                                        { item[api.type] }
                                    </li>
                                )
                        }
                    </ul>
                </div>
            </div>
        </div >
    )
}