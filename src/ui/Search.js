import Image from 'next/image'

export function SearchInput({ type = 'bordered', placeholder = 'Поиск...', value, onChange,onClick }) {

    // const findByName = () => { }

    return <div className={ `searchInput ${type}` }>
        <input
            className="searchInput__input"
            placeholder={ placeholder }
            value={ value }
            onChange={ onChange }
            type="text"
        />

        <button onClick={onClick} className={ `searchInput__btn ${type}` }>
            <Image src={ '/search.svg' } width='12px' height='12px' />
        </button>
    </div>
}