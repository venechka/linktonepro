
import { useRef } from "react";

export function InputAddress({ label, onChange, value, error, name, options }) {

    const inputRef = useRef()

    const copyValue = () => {
        navigator.clipboard.writeText(
            options?.artist_slug
                ? `${process.env.NEXT_PUBLIC_APP_URL}/${options['artist_slug']}/${value}`
                : `${process.env.NEXT_PUBLIC_APP_URL}/${value}`
        )

    }

    return <li className='currentInfo__right-input'>
        <span className={ `_input__label ${name}` }>{ label }</span>
        <div className={ `currentInfo__input ${error ? 'input__error' : ''}` }>
            <span className={ `currentInfo__input-left ${options?.artist_slug && 'exist'}` }>linkt.one/
                { options?.artist_slug ? `${options.artist_slug}/` : '' }
            </span>
            <input
                ref={ inputRef }
                name={ name }
                // placeholder={ options.artist_slug ? `${options.artist_slug}/` : '' }
                // className={ `` }
                type="text"
                value={ value }
                onChange={ onChange }
                maxLength={ 20 }
            />
            <span onClick={ copyValue } className="currentInfo__input-clone">
                <img src={ '/clone.svg' } alt="" />
            </span>
        </div>
    </li>
}