import { ContentHeader } from "components/ContentHeader";
import { Header } from "components/Header";
import { SideBar } from "components/SideBar";
import Head from "next/head";
import { createContext, useState } from "react";

export const HeaderContext = createContext({})

export function CommonLayout({ children, contentHeaderTitle, crumbs, isUpdate,title }) {

    const [isSideBarOpen, setIsSideBarOpen] = useState(false)

    return (
        <HeaderContext.Provider value={ { isSideBarOpen, setIsSideBarOpen } }>
            <div className="layout">
                <Head>
                    <title>{title}</title>
                </Head>
                <Header />
                <div className="layout__wrapper">
                    <SideBar />
                    <div className="layout__content">
                        <ContentHeader
                            title={ contentHeaderTitle }
                            crumbs={ crumbs }
                            isUpdate={isUpdate}
                        />
                        { children }
                    </div>
                </div>
            </div>
        </HeaderContext.Provider>
    )
}