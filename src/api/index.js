import axios from "axios"
import { useRouter } from "next/router"

export const $api = axios.create({
    baseURL: process.env.NEXT_PUBLIC_BACKEND_URL,
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
    },
    withCredentials: true,
})



export const swrPostFetcher = (route, data) => $api.post(route, data).then(res => res.data)
export const swrPostFetcherCatch = (route, data) =>
    // const router = useRouter()
    $api.post(route, data)
        .catch(res => res?.response.status
            //     {
            //     switch (res?.response?.status) {
            //         case 422:
            //             return { states: res.response.status, body: res.response.data.message }
            //         // return router.push('/asdawqdqwdq')
            //         case 500:
            //             return { states: res.response.status, body: `/500` }
            //         case 429:
            //             // too many requests
            //             return { states: res.response.status, body: `/429` }
            //         default:
            //             break;
            //     }
            // }
        )

export const swrGetFetcher = (route, data) => $api.get(route, data).then(res => res.data)
