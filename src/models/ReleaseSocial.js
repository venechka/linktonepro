export const releaseSocialLinks = [
    {
        id: 1, labelTxt: 'Yandex Music', icon: '/yandex.svg', type: 'yandex', iconBg: 'gray'
    },
    {
        id: 2, labelTxt: 'VK Music', icon: '/vkBlack.svg', type: 'vk', iconBg: 'gray'
    },
    {
        id: 3, labelTxt: 'OK Music', icon: '/okBlack.svg', type: 'ok', iconBg: 'gray'
    },
    {
        id: 4, labelTxt: 'Сбер Звук', icon: '/bank.svg', type: 'sber', iconBg: 'gray'
    },
    {
        id: 5, labelTxt: 'Tik Tok', icon: '/tiktok.svg', type: 'tiktok', iconBg: 'gray', flag: '/china.png'
    },
    {
        id: 6, labelTxt: 'Apple Music', icon: '/apple.svg', type: 'apple', iconBg: 'gray', flag: '/usa.png'
    },
    {
        id: 7, labelTxt: 'Spotify', icon: '/spotify.svg', type: 'spotify', iconBg: 'gray', flag: '/usa.png'
    },
    {
        id: 8, labelTxt: 'Youtube Music', icon: '/youtubeBlack.svg', type: 'youtube', iconBg: 'gray', flag: '/usa.png'
    },
    {
        id: 9, labelTxt: 'Deezer', icon: '/deezer.svg', type: 'deezer', iconBg: 'gray', flag: '/uk.png'
    },
]