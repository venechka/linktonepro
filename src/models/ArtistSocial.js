export const artistSocialLinks = [
    {
        id: 1, labelTxt: 'Веб-сайт', withIcon: true, withFlag: true, icon: '/web.svg', type: 'web', iconBg: 'web'
    },
    {
        id: 2, labelTxt: 'Telegram', withIcon: true, withFlag: true, icon: '/telegram.svg', type: 'telegram', iconBg: 'tg'
    },
    {
        id: 3, labelTxt: 'VK', withIcon: true, withFlag: true, icon: '/vk.svg', type: 'vk', iconBg: 'vk'
    },
    {
        id: 4, labelTxt: 'OK', withIcon: true, withFlag: true, icon: '/ok.svg', type: 'ok', iconBg: 'ok'
    },
    {
        id: 5, labelTxt: 'Youtube', withIcon: true, withFlag: true, icon: '/youtube.svg', type: 'youtube', iconBg: 'yt'
    },
]
