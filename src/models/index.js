import * as Yup from 'yup'

export const authSchema = Yup.object().shape({
    email: Yup.string()
        .required('Введите email')
        .matches(/^[\wi/g]{1,}@[\wi/g]{2,}\.[\wi/g]{2,5}$/),
    password: Yup.string()
        .required('Введите пароль')
        .matches(/^([\/\.\(\)\{\}\[\]\wi\d]){8,15}$/),
})
