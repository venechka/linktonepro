import { $api } from 'api'
import React, { createRef, useContext, useState } from 'react'

export function TabRowItem({ item, onChange, idx, remove, type }) {

    const tabThirdLabelRef = createRef()
    const tabThirdBtnRef = createRef()

    const [active, setActive] = useState(null)

    const loadBg = idx => async e => {
        onChange(idx, 'upload', type)(e)
        setActive(true)
        // e.target.value = ''
    }

    const removeBg = () => {
        onChange(idx, 'file', type)(null)
        setActive(false)
    }


    return <li className="tabs__item-row">
        <div className="tabs__row-first">
            <div className="tabs__row-top">
                <span>Банер #{ idx + 1 }</span>
            </div>
            <input
                value={ item['name'] }
                // onChange={ dispatch({ type: '', payload: e.target.value }) }
                onChange={ onChange(idx, 'name', type) }
                type="text"
            />
        </div>
        <div className="tabs__row-second">
            <div className="tabs__row-top">
                <span>Ссылка</span>
                <span onClick={ remove }>Удалить</span>
            </div>
            <input
                value={ item['link'] }
                onChange={ onChange(idx, 'link', type) }
                type="text"
            />
        </div>
        <div className="tabs__row-third">
            <div className="tabs__row-top">
                <span>Фон банера </span>
            </div>
            <div className="tabs__third-wrapper">
                <label ref={ tabThirdLabelRef } className="tabs__third-input">
                    { active && <span className="tabs__third-input-text">Поменять фон</span> }
                    { !active && <span className="tabs__third-input-text">Загрузить фон</span>}
                    <input onChange={ loadBg(idx) } type="file" />
                </label>
                { active && <button ref={ tabThirdBtnRef } onClick={ removeBg } className="tabs__third-btn">Удалить фон</button> }
            </div>
        </div>
    </li>
}