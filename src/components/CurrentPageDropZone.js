import Image from "next/image"
import { useEffect, useState } from "react"



export function CurrentPageDropZone({ title, text, setImg, error, setError, img, required, setMessage, dropZoneId }) {

    const [imgFile, setImgFile] = useState('')
    const [imgBlob, setImgBlob] = useState(img)

    useEffect(() => {
        setImgBlob(img)
    }, [img])


    const onDropHandler = (type) => e => {
        e.preventDefault()

        let file = type === 'input' ? e.target.files[0] : e.dataTransfer.files[0]
        console.log('file', file)
        e.target.value = ''

        if (!file) return

        const { type: fType, size } = file

        if (!fType.match(/image\/(jpg|jpeg|png|bmp)/g)) {
            setError(prev => ({ ...prev, [dropZoneId]: true }))
            setMessage('Неверный формат файла')
            return
        }


        if (size === 10 * (1024 ** 2)) {
            setError(prev => ({ ...prev, [dropZoneId]: true }))
            setMessage('Файл не соответствует размерам')
            return
        }

        setImgFile(file)
        setMessage(false)
        setError(prev => ({ ...prev, [dropZoneId]: false }))

        if (file) {
            setImg(file)
        }

        let reader = new FileReader()
        let url = reader.readAsDataURL(file)

        reader.onload = (e) => {
            setImgBlob(reader.result)
        }
    }

    return <div className={ `currentInfo__graphics ${error ? 'input__error' : ''}` }>
        <div className="currentInfo__graphics-left">
            <div className="currentInfo__left-icon">
                <Image src={ '/camera.svg' } width='83px' height='83px' alt="camera" />
            </div>
            <h2 className={ `currentInfo__left-title ${required && 'required'}` }>{ title }</h2>
            <p className="currentInfo__left-text">{ text }</p>
        </div>
        <div className="currentInfo__graphics-right" >
            <div className="currentInfo__right-dropzone">
                <label
                    onDragStart={ e => e.preventDefault() }
                    onDragOver={ e => e.preventDefault() }
                    onDrop={ onDropHandler('drop') }
                >
                    <input
                        // onChange={ loadImg }
                        onChange={ onDropHandler('input') }
                        type="file"
                        name=""
                        id=""
                    />
                    {
                        !imgBlob
                            ? <div className="currentInfo__dropzone-wrapper">
                                <div className="currentInfo__dropzone-icon">
                                    <Image
                                        src={ '/cloudUpload.svg' }
                                        width='83px'
                                        height='48px'
                                        alt="cloudUpload"
                                    />
                                    <span className="currentInfo__dropzone-text">
                                        <span>Перетащите / загрузите</span> изображение сюда
                                    </span>
                                </div>
                            </div>
                            : <div className="currentInfo__dropzone-img">
                                {/* <img src={ imgBlob } alt="" /> */ }
                                <Image
                                    loader={ () => imgBlob }
                                    src={ imgBlob }
                                    layout='fill'
                                    alt="cloudUpload"
                                    objectFit="contain"
                                    objectPosition={ 'center' }
                                    unoptimized
                                />
                            </div>
                    }
                </label>
            </div>
        </div>
    </div>
}