import { $api } from 'api'
import Image from 'next/image'
import React, { useState } from 'react'
import { v4 } from 'uuid'
// import { mutate } from 'swr'


// function LoadStories({ isNotValidFormat, onDropHandler }) {
function LoadStories({ stories, setStories, essence, essence_id, mutate, setUnProcessableContent }) {

    // const [unProcessableContent, setUnProcessableContent] = useState(null)
    const [isNotValidFormat, setIsNotValidFormat] = useState(null)

    const onDropHandler = (type) => e => {
        e.preventDefault()
        let file = type === 'input' ? e.target.files[0] : e.dataTransfer.files[0]

        if (!file) return

        if (file.type !== 'video/mp4') {
            setUnProcessableContent('Формат должен быть mp4')
            setIsNotValidFormat(true)
            return
        }

        if (stories.length === 3) {
            setUnProcessableContent('Вы не можите загрузить больше 3х сторис')
            return
        }

        let reader = new FileReader()



        if (file.size <= 15 * (1024 ** 2)) {
            e.target.value = ''
            let url = reader.readAsDataURL(file)

            const formData = new FormData()

            reader.onload = async (e) => {
                setUnProcessableContent(null)
                setIsNotValidFormat(false)
                setStories(prev => [...prev, { id: v4(), file, deleted: false }])
                // formData.append(essence, essence_id)
                // formData.append('story', file)
                // await $api.post(`/api/stories`, formData)
                // mutate()
            }
        }
    }

    return (
        <div className="tabs__item-stories">
            <span>Видео с соотношением сторон 9:16.<br/>  mp4 (max 15мб)</span>
            <div className={ `currentInfo__right-dropzone ${isNotValidFormat && 'input__error'}` }>
                <label
                    onDragStart={ e => e.preventDefault() }
                    onDragOver={ e => e.preventDefault() }
                    onDrop={ onDropHandler('drop') }
                >
                    <input
                        onChange={ onDropHandler('input') }
                        type="file"
                        name=""
                        id=""
                    />
                    <div className="currentInfo__dropzone-wrapper">
                        <div className="currentInfo__dropzone-icon">
                            <Image
                                src={ '/cloudUpload.svg' }
                                width='83px'
                                height='48px'
                                alt='cloudUpload'
                            />
                            <span className="currentInfo__dropzone-text">
                                <span>Перетащите / загрузите</span> видео сюда
                            </span>
                        </div>
                    </div>
                </label>
            </div>

        </div>
    )
}

export default LoadStories