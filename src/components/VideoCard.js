import Image from 'next/image'
import React from 'react'
import { Button } from 'ui/Button'
import { getVideoImg } from 'utils'


function VideoCard({ video, app, unTieVideo, children, videos }) {

    return (
        <div
            key={ video.id }
            className={ `tabs__dropzone-card ${videos && (videos.find(v => v.id === video.id)?.id && 'exist')}` }
        >
            <div className="tabs__dropzone-card-img">
                {/* <img src={ `https://img.youtube.com/vi/${video.link}/hqdefault.jpg` } alt="" /> */}
                <Image
                    layout='fill'
                    objectFit='cover'
                    objectPosition='center'
                    loader={ () => getVideoImg(video.link) }
                    src={ getVideoImg(video.link) }
                    alt='img'
                />
            </div>
            <div className="tabs__dropzone-card-body">
                <span className="tabs__dropzone-card-type">
                    {
                        app
                            ? app.dictionaries.video_types
                                .find(t => t.id === video.video_type_id).name
                            : ''
                    }
                </span>
                <span className="tabs__dropzone-card-title">
                    { video.name }
                </span>
            </div>
            {
                children
            }
        </div>
    )
}

export default VideoCard