import { Autocomplete, TextField } from "@mui/material"
import { Formik, isObject, useFormik, useFormikContext } from "formik"
import { Form as FormikForm } from "formik"
import Image from "next/image"
import { useContext, useEffect, useRef, createRef } from "react"
import { CustomSelect } from "ui/CustomSelect"
import { Input } from "ui/Input"
import { InputAddress } from "ui/InputAddress"
import { Select } from "ui/Select"
import { MainFormContext } from "utils"
import { ArtistFormContext } from "./ArtistFormpage"
import { DateTimeInput } from "./DateTimeInput"



const FormikValues = ({ setInputsValues }) => {

    const { values, errors } = useFormikContext()

    useEffect(() => {
        setInputsValues({ ...values, errors: { ...errors } })
    }, [values, errors])

    return null
}

export default function Form({ schema, defaultValues }) {

    const { setInputsValues } = useContext(MainFormContext)

    return (
        <div>
            <Formik
                validationSchema={ schema }
                initialValues={ defaultValues }
                enableReinitialize
            >
                { ({
                    values,
                    errors,
                    handleChange,
                    setFieldValue
                    /* and other goodies */
                }) => (
                    <>
                        { Object.keys(schema.fields).map((field, index) => {
                            if (schema.fields[field].spec?.meta?.inputAddress) return <InputAddress
                                key={ index }
                                name={ field }
                                label={ schema.fields[field].spec.label }
                                value={ values[field] }
                                onChange={ handleChange }
                                error={ errors[field] }
                                options={ schema.fields[field].spec.meta }
                            />
                            if (schema.fields[field].spec?.meta?.select) return <Select
                                key={ index }
                                name={ field }
                                title={ schema.fields[field].spec.label }
                                value={ values[field] }
                                dropDownItems={ schema.fields[field].spec.meta.dropDownItems }
                                // onChange={ handleChange }
                                onChange={ handleChange }
                                error={ errors[field] }
                            />

                            if (schema.fields[field].spec?.meta?.textArea) return <li
                                key={ index }
                                className="currentInfo__right-input">
                                <span className={ `${field}` }>{ schema.fields[field].spec.label }</span>
                                <textarea
                                    name={ field }
                                    value={ values[field] }
                                    // onChange={ handleChange }
                                    onChange={ handleChange }
                                    maxLength={ field === 'tracks' ? 1000 : 500 }
                                ></textarea>
                            </li>
                            if (schema.fields[field].spec?.meta?.dateTime) return <DateTimeInput
                                key={ index }
                                label={ schema.fields[field].spec.label }
                                dateValue={ values[field] }
                                name={ field }
                                error={ !!errors[field] }
                                setFieldValue={ setFieldValue }
                            // setFieldValue={ setFieldValue }
                            />

                            if (schema.fields[field].spec?.meta?.youtubeLink) return < Input
                                key={ index }
                                name={ field }
                                withIcon
                                icon={ '/youtube.svg' }
                                iconBg={ 'yt' }
                                labelTxt={ schema.fields[field].spec.label }
                                value={ values[field] }
                                // onChange={ handleChange }
                                onChange={ handleChange }
                                isConcert={ schema.fields[field].spec.meta.isConcert }
                                error={ !!errors[field] }
                            />
                            if (schema.fields[field].spec?.meta?.country) return <CustomSelect
                                key={ index }
                                name={ field }
                                value={ values[field] }
                                // setFieldValue={ setFieldValue }
                                setFieldValue={ setFieldValue }
                                label={ schema.fields[field].spec.label }
                                dropDownItems={ schema.fields[field].spec.meta.dropDownItems }
                                error={ !!errors[field] }
                                api={ {
                                    route: schema.fields[field].spec.meta.route,
                                    type: schema.fields[field].spec.meta.type,
                                } }
                            />
                            if (schema.fields[field].spec?.meta?.city) return <CustomSelect
                                key={ index }
                                name={ field }
                                value={ values[field] }
                                // setFieldValue={ setFieldValue }
                                setFieldValue={ setFieldValue }
                                label={ schema.fields[field].spec.label }
                                dropDownItems={ schema.fields[field].spec.meta.dropDownItems }
                                error={ !!errors[field] }
                                api={ {
                                    route: schema.fields[field].spec.meta.route,
                                    type: schema.fields[field].spec.meta.type,
                                    country: values['country']
                                } }
                            />
                            return < Input
                                key={ index }
                                name={ field }
                                labelTxt={ schema.fields[field].spec.label }
                                value={ values[field] }
                                // onChange={ handleChange }
                                onChange={ handleChange }
                                options={schema.fields[field].spec.meta}
                                error={ !!errors[field] }
                            />
                        }) }
                        <FormikValues setInputsValues={ setInputsValues } />
                    </>
                ) }
            </Formik>
        </div >
    )
}