import Image from "next/image"
import { useEffect, useRef, useState } from "react"



export function DateTimeInput({ dateValue, name, setFieldValue, error,label }) {

    const setInputValue = (field) => e => {
        setFieldValue(name, { ...dateValue, [field]: e.target.value })
    }

    return (
        <li className="currentInfo__right-input date">
            <span>{ label ? label : 'Дата и время релиза'}</span>
            <div className="currentInfo__input-wrapper">
                <div className={ `currentInfo__input-date ${error ? 'input__error' : ''}` }>
                    <span>
                        <Image src={ '/calendar.svg' } width='16px' height='16px' alt="calendar" />
                    </span>
                    <input
                        type="date"
                        value={ dateValue['date'] }
                        max={ '9999-12-31' }
                        onChange={ setInputValue('date') }
                    />
                </div>
                <div className="currentInfo__input-time">
                    <span>
                        <Image src={ '/clock.svg' } width='16px' height='16px' alt="clock" />
                    </span>
                    <input
                        type="time"
                        value={ dateValue['time'] }
                        onChange={ setInputValue('time') }
                    />
                </div>
            </div>
        </li>
    )
}