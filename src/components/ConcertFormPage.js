import { $api, swrGetFetcher, swrPostFetcherCatch } from "api";
import { CurrentPageDropZone } from "components/CurrentPageDropZone";
import { CurrentPageMain } from "components/CurrentPageMain";
import Image from "next/image";
import { useRouter } from "next/router";
import { CONCERTS } from "pathes/utils";
import { useContext, useEffect, useState } from "react";
import { Button } from "ui/Button";
import Form from "./Form";
import { useFormik } from 'formik';
import * as Yup from 'yup'
import { useLoading } from "hooks/useLoading";
import { ContentLoader } from "./ContentLoader";
import { ConfirmDeleteModal } from "./ConfirmDeleteModal";
import { AppContext } from "pages/_app";
import { v1 } from "uuid";
import { useTags } from "hooks/useTags";
import { useRequest } from "hooks/useRequest";
import NotValidContent from "./NotValidContent";
import useSWR from "swr";
import { CommonLayout } from "layouts/Common";
import Tags from "./Tags";
import Stories from "./Stories";
import LoadStories from "./LoadStories";
import { v4 } from "uuid";
import { MainFormContext } from "utils";



export function ConcertFormPage({ update }) {

    const router = useRouter()
    const { id, concert_id } = router.query

    const { data: artist } = useSWR(id ? `/api/artists/${id}` : null, swrGetFetcher)
    const { data: concert, mutate: _mutate, error } = useSWR(concert_id ? `/api/concerts/${concert_id}` : null, swrGetFetcher)
    if (error?.response?.status === 404) {
        router.push(`/artists/${id}/concerts`)
    }


    const { app, mutate } = useContext(AppContext)
    const { tags, addTags, addTag, removeTag, getTagsValue } = useTags()

    const { set } = useRequest()

    const [imgLogo, setImgLogo] = useState('')
    // const { isLoading } = useLoading(concert)

    const [defaultValues, setDefaultValues] = useState(
        {
            publish_date: { date: '', time: '20:00' },
            ticket_links: '',
            youtubeLink: '',
            country: '',
            city: '',
            venue: '',
            type: ''
        }
    )
    const [description, setDescription] = useState('')
    const setDescriptionValue = e => setDescription(e.target.value)

    const [stories, setStories] = useState([])
    const [deletedStories, setDeletedStories] = useState([])

    const [isLoading, setIsLoading] = useState(false)

    const regexp = /^(http|https):\/\/.+$/
    const schema = Yup.object().shape({
        publish_date: Yup.object()
            .meta({
                dateTime: true,
                concert: true
            })
            .shape({
                date: Yup.string().required(),
                time: Yup.string(),
            })
            .label('Дата и время начала концерта'),
        // slug: Yup.string()
        //     .meta({
        //         inputAddress: true,
        //         artist_slug: artist ? artist.slug : ''
        //     })
        //     .test('onluEng', 'Некорректные символы', (value) => {
        //         const regex = /^[a-zA-Z0-9\-]{5,}$/
        //         return regex.test(String(value).toLowerCase())
        //     })
        //     .label('Адрес концерта')
        //     .required('Укажите адрес концерта'),
        ticket_links: Yup.string()
            .required('Укажите ссылку на билеты')
            .test('checkLink', 'Неверная ссылка', (value) => {
                if (!regexp.test(String(value).toLowerCase()) && value?.length) {
                    setUnProcessableContent('Ссылка должна содержать http(s)://')
                    return false
                }
                setUnProcessableContent(null)
                return true
            })
            .label('Ссылка на билеты'),
        youtubeLink: Yup.string()
            .notRequired()
            .meta({
                youtubeLink: true,
                isConcert: true
            })
            .nullable()
            .label('Ссылка на видео'),
        country: Yup.string()
            .required('Введите страну')
            .meta({
                country: true,
                route: '/api/app/searchCountry',
                type: 'country'
            })
            .label('Страна'),
        city: Yup.string()
            .required('Введите город')
            .meta({
                city: true,
                route: '/api/app/searchCity',
                type: 'city',
            })
            .label('Населенный пункт'),
        venue: Yup.string()
            .required('Введите площадку')
            .label('Площадка'),
        type: Yup.string()
            .meta({
                select: true,
                dropDownItems: app ? app.dictionaries.concert_types : []
            })
            .required('Выбирите тип выступления')
            .label('Тип выступления'),
    })

    useEffect(() => {
        if (update && concert) {
            const {
                video_link,
                ticket_links,
                venue,
                country,
                city,
                concert_type_id,
                description: _desc,
                start_datetime,
                tags,
                slug,
                stories: _stories,
                is_banned
            } = concert


            if (is_banned) setUnProcessableContent('Этот концерт заблокирован')

            const [date, time] = start_datetime.split(/\s/g)

            const parsedTicketLinks = ticket_links
            const parsedTags = tags ? JSON.parse(tags) : []

            setStories(_stories)

            addTags(parsedTags.map(_tag => ({ id: v4(), title: _tag })))
            setDescription(_desc ?? '')

            setDefaultValues(
                {
                    youtubeLink: video_link ?? '',
                    ticket_links: parsedTicketLinks,
                    venue,
                    country,
                    city,
                    type: concert_type_id,
                    publish_date: { date, time },
                    slug
                }
            )
        }
    }, [concert])// eslint-disable-line react-hooks/exhaustive-deps


    const [errors, setErrors] = useState([])
    const [unProcessableContent, setUnProcessableContent] = useState(null)



    const [inputsValues, setInputsValues] = useState(null)
    const saveArtist = async data => {

        const { errors, ...other } = inputsValues

        for (const key in other) {
            if (!other[key] && !key.match(/(youtubeLink)/g)) {
                setUnProcessableContent('Заполните все поля')
                return
            }
        }


        const formData = new FormData()
        const { date, time } = other['publish_date']


        let storiesArr = []
        if (stories.length) {
            const s = stories.filter(s => !+s.id)
            storiesArr.push(...s.reduce((prev, next, index) => [...prev, { id: 'new', file_index: index }], []))
            storiesArr.forEach((item, index) => formData.append('stories_files[]', s[index].file))
        }

        if (deletedStories.length) {
            storiesArr.push(...deletedStories.reduce((prev, next) => [...prev, { id: next.id, delete: true }], []))
        }

        if (storiesArr.length && update) {
            formData.append('stories', JSON.stringify(storiesArr))
        }

        formData.append('artist_id', id)
        formData.append('start_datetime', `${date} ${time}`)
        formData.append('ticket_links', other['ticket_links'])

        other['youtubeLink'].length && formData.append('video_link', other['youtubeLink'])

        formData.append('country', other['country'])
        formData.append('city', other['city'])
        formData.append('venue', other['venue'])
        formData.append('concert_type_id', other['type'])
        formData.append('description', description ?? '')
        formData.append('tags', JSON.stringify(getTagsValue()))
        formData.append('slug', v4())

        if (inputsErrors['cover']) return
        if (imgLogo) formData.append('poster', imgLogo)

        setIsLoading(true)
        if (update) {
            formData.append('_method', 'PUT')
            const error = await set(`/api/concerts/${concert_id}`, formData, `/artists/${id}/concerts`)
            setIsLoading(false)
            if (error) {
                setIsLoading(false)
                setUnProcessableContent(error)
            }
            mutate()
            return
        }

        const error = await set(`/api/concerts`, formData, `/artists/${id}/concerts`)
        setIsLoading(false)
        if (error) {
            setIsLoading(false)
            setUnProcessableContent(error)
        }
        mutate()
    }

    const [inputsErrors, setInputsErrors] = useState({
        cover: false
    })

    const [isDelete, setIsDelete] = useState(false)
    const removeConcert = async () => {
        if (isDelete) {
            setIsDelete(false)
            await $api.delete(`/api/concerts/${concert_id}`)
            router.push(`/artists/${id}/concerts`)
            mutate()
            return
        }
        setIsDelete(true)
    }

    const cancel = () => {
        if (isDelete) {
            setIsDelete(false)
            return
        }
        router.push(`/artists/${id}/concerts`)
    }

    const addTagByEnter = e => {

        if (e.code === 'Enter' || e.code === 'Space') {
            addTag(e)
        }
    }

    return (
        <CommonLayout
            contentHeaderTitle={ 'Редактирование концерта' }
            crumbs={ [
                {
                    id: 0, title: 'Концерт'
                },
            ] }
            isUpdate={ update && { link: `${process.env.NEXT_PUBLIC_APP_URL}/${artist?.slug}/${concert?.slug}` } }
            title={ `Редактирование концерта` }
        >
            {
                unProcessableContent && <NotValidContent error={ unProcessableContent } />
            }
            <div className="currentInfo">
                <div className="currentInfo__wrapper">
                    <div className="currentInfo__inner">
                        <MainFormContext.Provider value={ { setInputsValues } }>
                            <CurrentPageMain
                                esenceTitle={ 'Добавьте ключевую информацию об артисте' }
                                page={ CONCERTS }
                                defaultValues={ defaultValues }
                                schema={ schema }
                            />
                        </MainFormContext.Provider>
                        <CurrentPageDropZone
                            title={ 'Афиша' }
                            text={ 'Загрузите обложку концерта формат и размер максимальный.В формате JPG или PNG (max 10Mb)' }
                            setImg={ setImgLogo }
                            img={ concert?.poster }
                            setError={ setInputsErrors }
                            error={ inputsErrors['cover'] }
                            setMessage={ setUnProcessableContent }
                            dropZoneId='cover'
                        />
                        <div className="currentInfo__main">
                            <div className="currentInfo__main-left">
                                <div className="currentInfo__left-icon">
                                    {/* <Image src={ '/box.svg' } width='83px' height='83px' /> */ }
                                </div>
                                <h2 className="currentInfo__left-title">Промо</h2>
                                <p className="currentInfo__left-text"></p>
                            </div>
                            <div className="currentInfo__main-right">
                                <ul className="currentInfo__right-inputs">
                                    {
                                        <Tags
                                            tags={ tags }
                                            removeTag={ removeTag }
                                            addTag={ addTag }
                                            addTagByEnter={ addTagByEnter }
                                        />
                                    }
                                    <div className="tabs__item-caption">
                                        <span>Описание <br />(max 500 символов)</span>
                                        <textarea
                                            value={ description }
                                            onChange={ setDescriptionValue }
                                            maxLength={ 500 }
                                        ></textarea>
                                    </div>

                                    {
                                        <LoadStories
                                            stories={ stories }
                                            setStories={ setStories }
                                            setUnProcessableContent={ setUnProcessableContent }
                                            mutate={ _mutate }
                                            essence='concert_id'
                                            essence_id={ concert_id }
                                        />
                                    }
                                    <Stories
                                        stories={ stories }
                                        setDeletedStories={ setDeletedStories }
                                        mutate={ _mutate }
                                        setStories={ setStories }
                                    />
                                </ul>
                            </div>
                        </div>
                        <div className="currentInfo__btns">
                            <div className="currentInfo__btns-left">
                                {/* <Button onClick={ formik.handleSubmit } txt='Сохранить' color='primary' /> */ }
                                {
                                    !isLoading
                                        ? <Button onClick={ saveArtist } txt='Сохранить' color='primary' />
                                        : <ContentLoader />
                                }
                                <Button onClick={ cancel } txt='Отменить' color='cancel' />
                            </div>
                            {
                                update && <Button onClick={ removeConcert } txt='Удалить концерт' color='delete' />
                            }

                        </div>
                    </div>
                    {
                        isDelete && <ConfirmDeleteModal esence={ 'концерт' }>
                            <Button onClick={ removeConcert } txt='Удалить концерт' color='delete' />
                            <Button onClick={ cancel } txt='Отменить' color='cancel' />
                        </ConfirmDeleteModal>
                    }
                </div>
            </div>
        </CommonLayout>

    )
}