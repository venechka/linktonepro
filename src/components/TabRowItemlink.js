import Image from 'next/image'
import { ARTISTS } from 'pathes/utils'

export function TabRowItemLink({ page, item, setAddedItems, id, idx, setField, type,error }) {
    // export function TabRowItemLink({ page, jobValue, nameValue, linkValue, onChange }) {

    const removeItem = () => setAddedItems(id)

    return <li className="tabs__item-row">
        <div className="tabs__row-first">
            <div className="tabs__row-top">
                <span>Позиция</span>
            </div>
            <input
                value={ item['job'] }
                onChange={ setField(idx, 'job', type) }
                type="text"
                name=""
                id=""
            />
        </div>
        <div className="tabs__row-second">
            <div className="tabs__row-top">
                <span>Имя</span>
                {
                    page === ARTISTS
                        ? <span onClick={ removeItem }>Удалить</span>
                        : null
                }

            </div>
            <input
                value={ item['name'] }
                onChange={ setField(idx, 'name', type) }
                type="text"
                name=""
                id=""
            />
        </div>
        <div className="tabs__row-third">
            {
                page === ARTISTS
                    ? <div
                        className={`tabs__third-wrapper ${item['error'] && 'input__error'}`}
                    >
                        <span>
                            <Image src={ '/vkBlack.svg' } width='16px' height='16px'  alt='vkBlack'/>
                        </span>
                        <input
                            value={ item['link'] }
                            onChange={ setField(idx, 'link', type,error) }
                            type="text"
                        />
                    </div>
                    : <>
                        <div className="tabs__row-top">
                            <span>Instagram</span>
                            <span onClick={ removeItem }>Удалить</span>
                        </div>
                        <input
                            value={ item['link'] }
                            onChange={ setField(idx, 'link', type) }
                            type="text"
                            name=""
                            id=""
                        />
                    </>
            }

        </div>
    </li>
}