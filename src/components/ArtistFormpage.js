import { $api, swrGetFetcher } from "api";
import { CurrentPageDropZone } from "components/CurrentPageDropZone";
import { CurrentPageMain } from "components/CurrentPageMain";
import { TabRowItem } from "components/TabRowItem";
import { TabRowItemLink } from "components/TabRowItemlink";
import { CommonLayout } from "layouts/Common";
import { useRouter } from "next/router";
import { ARTISTS } from "pathes/utils";
import { createContext, useContext, useEffect, useState } from "react";
import { Button } from "ui/Button";
import { Input } from "ui/Input";
import { useFormik } from 'formik';
import Form from "./Form";
import { FormTabs } from "./FormTabs";
import FormTab from "./FormTab";
import { ContentLoader } from "./ContentLoader";
import { ConfirmDeleteModal } from "./ConfirmDeleteModal";
import { AppContext } from "pages/_app";
import useSWR from "swr";
import { useMembers } from "hooks/useMembers";
import { useBanners } from "hooks/useBanners";
import { useArtistSchema } from "hooks/useArtistSchema";
import { useRequest } from "hooks/useRequest";
import NotValidContent from "./NotValidContent";
import { artistSocialLinks } from "models/ArtistSocial";
import { appErros, MainFormContext } from "utils";



export const ArtistFormContext = createContext(null)
export function ArtistFormPage({ update }) {

    const router = useRouter()
    const { id } = router.query

    const { data: artist, error } = useSWR(id ? `/api/artists/${id}` : null, swrGetFetcher)

    if (error?.response?.status === 404) {
        router.push(`/`)
    }

    const { app, mutate } = useContext(AppContext)

    const { members, addMembers, addMember, removeMember, setMemberField } = useMembers()
    const { banners, addBanners, addBanner, removeBanner, setBannerField, deletedBanners } = useBanners()

    const { set } = useRequest()

    const { schema } = useArtistSchema(app ? app.dictionaries.artist_types : [], app ? app.dictionaries.genres : [],)

    const [avatarImg, setAvatarImg] = useState('')
    const [logoImg, setLogoImg] = useState('')

    const [unProcessableContent, setUnProcessableContent] = useState(null)

    const [isLoading, setIsLoading] = useState(false)

    const [socialLinksValue, setSocialLinksValue] = useState({
        'web': { value: '', error: false },
        'telegram': { value: '', error: false },
        'vk': { value: '', error: false },
        'ok': { value: '', error: false },
        'youtube': { value: '', error: false }
    })

    const regexp = /^(http|https):\/\/.+$/
    const setLinkValue = field => e => {
        setSocialLinksValue(prev =>
        ({
            ...prev, [field]:
            {
                ...prev[field],
                value: e.target.value,
                error: e.target.value && !regexp.test(String(e.target.value).toLowerCase())
            }
        }))
        setUnProcessableContent(
            (!regexp.test(String(e.target.value).toLowerCase()) && e.target.value.length)
                ? 'Ссылка должна содержать в себе http(s)://'
                : null
        )
    }

    const [defaultValues, setDefaultValues] = useState({ name: '', slug: '', artist_types_id: '', genres_id: '', country: '', city: '', description: '' })

    useEffect(() => {
        if (update && artist) {
            const {
                name,
                artist_types_id,
                genres_id,
                country,
                city,
                description,
                social_links,
                members: artist_members,
                banners: artist_banners,
                slug,
                is_banned
            } = artist

            if (is_banned) setUnProcessableContent('Этот артист забанен')

            if (social_links) {
                const parseSocialLinks = Object.keys(JSON.parse(social_links)).length
                    ? Object.keys(JSON.parse(social_links)).reduce((prev, next) => ({
                        ...prev, [next]: { value: JSON.parse(social_links)[next], error: false }
                    }), {})
                    : socialLinksValue

                setSocialLinksValue(
                    // Object.keys(parseSocialLinks).reduce((prev, next) => ({ ...prev, [next]: { value: parseSocialLinks[next], error: false } }), {})
                    // socialLinksValue
                    parseSocialLinks
                )
            }

            // console.log(Object.keys(socialLinksValue))


            addMembers(JSON.parse(artist_members))

            addBanners(artist_banners.length ? artist_banners.map(banner => ({ ...banner, isChanged: true, wasDeleted: false })) : [])

            setDefaultValues({
                name,
                artist_types_id,
                genres_id,
                country,
                city,
                slug,
                description: description ? description : '',
            })
        }

    }, [artist])// eslint-disable-line react-hooks/exhaustive-deps

    // console.log('v2', Object.keys(socialLinksValue))

    const [inputsValues, setInputsValues] = useState(null)


    const saveArtist = async () => {

        const formData = new FormData()

        const { errors, ...other } = inputsValues

        for (const key in other) {
            if (!other[key] && !key.match(/(description)/g)) {
                setUnProcessableContent('Заполните все поля и загрузите аватар артиста')
                return
            }
        }

        if (!logoImg && !update) {
            setInputsErrors(prev => ({ ...prev, 'avatar': true }))
            return
        }

        if (inputsErrors['avatar'] || inputsErrors['cover']) {
            return
        }

        for (const key in other) {
            formData.append(key, other[key])
        }

        for (const key in members) {
            if (!members[key]['job'].length || !members[key]['name'].length) {
                setUnProcessableContent('Заполните поля участников')
                return
            }
            if (members[key]['error']) {
                setUnProcessableContent('Неверный формат ссылки соцсети Вконтакте')
                return
            }
        }

        formData.append('members', JSON.stringify(members))
        !update && formData.append('owner_id', app.user.id)

        for (const key in socialLinksValue) {
            if (socialLinksValue[key]['error']) {
                setUnProcessableContent('Неверный формат ссылок')
                return
            }
        }

        formData.append('social_links', JSON.stringify(
            Object.keys(socialLinksValue).reduce((acc, next) => ({ ...acc, [next]: socialLinksValue[next]['value'] }), {}))
        )

        // console.log(JSON.stringify(
        //     Object.keys(socialLinksValue).reduce((prev, next) => ({ ...prev, [next]: socialLinksValue[next]['value'] }), {})
        // ));
        // return
        if (logoImg) formData.append('logo', logoImg)
        if (avatarImg) formData.append('picture', avatarImg)

        if (banners) {
            let arr = []
            let pictureIndex = -1
            for (const key in banners) {
                if (!banners[key].isChanged) {
                    setUnProcessableContent('Заполните поля банера')
                    return
                }

                if (!banners[key].file && !banners[key].upload) {
                    setUnProcessableContent('Загрузите фон банера')
                    return
                }

                if (banners[key].upload) {
                    formData.append('banners_pictures[]', banners[key].upload)
                    pictureIndex++
                }
                arr.push(
                    {
                        id: +banners[key].id ? banners[key].id : 'new',
                        name: banners[key].name,
                        link: banners[key].link,
                        picture_index: banners[key].upload ? Object.keys(formData.getAll('banners_pictures[]'))[pictureIndex] : -1
                    }
                )
            }

            if (deletedBanners.length) arr.push(...deletedBanners)
            formData.append('banners', JSON.stringify(arr))
        }

        setIsLoading(true)
        if (update) {
            formData.append('_method', 'PUT')
            const error = await set(`/api/artists/${artist.id}`, formData, '/')
            setIsLoading(false)
            if (error) {
                setIsLoading(false)
                setUnProcessableContent(appErros['ru'][error])
            }
            mutate()
            return
        }

        const error = await set(`/api/artists`, formData, '/')
        setIsLoading(false)
        if (error) {
            setIsLoading(false)
            setUnProcessableContent(appErros['ru'][error])
        }
        mutate()
    }

    const [inputsErrors, setInputsErrors] = useState({ cover: false, avatar: false })

    const [isDelete, setIsDelete] = useState(false)
    const removeArtist = async () => {
        await $api.delete(`/api/artists/${artist.id}`)
        router.push('/')
        mutate()
    }

    const cancel = () => {
        if (isDelete) {
            setIsDelete(false)
            return
        }
        router.push('/')
    }

    if (!app) return <ContentLoader />

    return (
        <CommonLayout
            contentHeaderTitle={ 'Профиль артиста' }
            isUpdate={ update && { link: `${process.env.NEXT_PUBLIC_APP_URL}/${artist?.slug}` } }
            title={ `Профиль артиста ${artist?.slug}` }
        >
            {
                unProcessableContent && <NotValidContent error={ unProcessableContent } />
            }

            <div className="currentInfo">
                <div className="currentInfo__wrapper">
                    <div className="currentInfo__inner">
                        <MainFormContext.Provider value={ { setInputsValues } }>
                            <CurrentPageMain
                                esenceTitle='Добавьте ключевую информацию об артисте'
                                page={ ARTISTS }
                                schema={ schema }
                                defaultValues={ defaultValues }
                            />
                        </MainFormContext.Provider>

                        <CurrentPageDropZone
                            title={ 'Аватар' }
                            text={ 'Загрузите логотип артиста 1:1. JPG, PNG (max 10Mb)' }
                            error={ inputsErrors['avatar'] }
                            setImg={ setLogoImg }
                            setError={ setInputsErrors }
                            required
                            setMessage={ setUnProcessableContent }
                            img={ artist ? artist.logo : '' }
                            dropZoneId='avatar'
                        />
                        <CurrentPageDropZone
                            title={ 'Обложка' }
                            text={ 'Загрузите обложку профиля артиста 1920:600. JPG, PNG (max 10Mb)' }
                            setImg={ setAvatarImg }
                            error={ inputsErrors['cover'] }
                            setError={ setInputsErrors }
                            setMessage={ setUnProcessableContent }
                            img={ artist ? artist.picture ?? '' : '' }
                            dropZoneId='cover'
                        />
                        <div className="currentInfo__tabs">
                            <FormTabs >
                                <FormTab label={ 'Ссылки' }>
                                    {
                                        artistSocialLinks.map(link =>
                                            <Input
                                                key={ link.id }
                                                labelTxt={ link.labelTxt }
                                                withIcon
                                                withFlag
                                                icon={ link.icon }
                                                value={ socialLinksValue[link.type]['value'] }
                                                onChange={ setLinkValue(link.type) }
                                                iconBg={ link.iconBg }
                                                error={ socialLinksValue[link.type]['error'] }
                                            />
                                        )
                                    }
                                </FormTab>
                                <FormTab label={ 'Участники' }>
                                    {
                                        members && members.map((member, idx) =>
                                            <TabRowItemLink
                                                key={ member.id }
                                                page={ ARTISTS }
                                                id={ member.id }
                                                idx={ idx }
                                                item={ member }
                                                type={ 'member' }
                                                setField={ setMemberField }
                                                setAddedItems={ removeMember(member.id) }
                                                error={ setUnProcessableContent }
                                            />
                                        )

                                    }
                                    <Button
                                        color="primary"
                                        txt='+ Добавить'
                                        onClick={ addMember }
                                    />
                                </FormTab>
                                <FormTab label={ 'Банеры' }>

                                    {
                                        banners && banners.map((banner, idx) =>
                                            <TabRowItem
                                                key={ banner.id }
                                                item={ banner }
                                                idx={ idx }
                                                type={ 'banner' }
                                                onChange={ setBannerField }
                                                remove={ removeBanner(banner.id) }
                                            />
                                            // : null
                                        )
                                    }
                                    {
                                        banners.length === 3
                                            ? null
                                            : <Button
                                                color="primary"
                                                txt='+ Добавить'
                                                onClick={ addBanner }
                                            />
                                    }

                                </FormTab>
                            </FormTabs>
                        </div>
                        <div className="currentInfo__btns">
                            <div className="currentInfo__btns-left">
                                {
                                    !isLoading
                                        ? <Button
                                            // onClick={ formik.handleSubmit }
                                            onClick={ saveArtist }
                                            txt='Сохранить'
                                            color='primary'
                                        />
                                        : <ContentLoader />
                                }

                                <Button onClick={ cancel } txt='Отменить' color='cancel' />
                            </div>
                            {
                                update && <Button onClick={ () => setIsDelete(!isDelete) } txt='Удалить артиста' color='delete' />
                            }
                        </div>
                        {
                            isDelete && <ConfirmDeleteModal esence={ 'артиста' }>
                                <Button onClick={ removeArtist } txt='Удалить артиста' color='delete' />
                                <Button onClick={ cancel } txt='Отменить' color='cancel' />
                            </ConfirmDeleteModal>
                        }
                    </div>
                </div>
            </div>
        </CommonLayout>
    )
}