import React from 'react'

function NotValidContent({ error }) {
    return (
        <div
            className='not-valid-content'
            style={ {
                position: 'absolute',
                left:0,
                right:0,
                padding:10,
                backgroundColor:'red',
                color:'#fff',
                zIndex:3
            } }
        >
            { error }
        </div>
    )
}

export default NotValidContent