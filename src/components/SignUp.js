import { useAuth } from "hooks/useAuth"
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import { ContentLoader } from "./ContentLoader"
import * as Yup from 'yup'
import { useFormik } from "formik"
import Image from "next/image"




export function SignUp() {

    const router = useRouter()

    const signIn = () => router.push('/login')

    const { register } = useAuth()

    const [errors, setErrors] = useState({
        email: '',
        password: ''
    })
    const [isLoading, setIsLoading] = useState(false)

    const signUp = async data => {
        setIsLoading(true)
        await register({ ...data, setErrors })
        setIsLoading(false)
    }

    const schema = Yup.object().shape({
        name: Yup.string()
            .required('Введите имя')
            .min(3),
        lastname: Yup.string()
            .required('Введите фамилию')
            .min(3),
        email: Yup.string()
            .required('Введите email'),
        password: Yup.string()
            .required('Введите пароль')
            .matches(/^([\/\.\(\)\{\}\[\]\d\w]){8,15}$/),
        password_confirmation: Yup.string()
            .required('Подтвердите пароль')
            .matches(/^([\/\.\(\)\{\}\[\]\d\w]){8,15}$/)
    })

    const [defaultValues, setDefaultValues] = useState(null)

    const formik = useFormik({
        validationSchema: schema,
        initialValues: defaultValues ?? { name: '', lastname: '', email: '', password: '', password_confirmation: '' },
        enableReinitialize: true,
        onSubmit: signUp
    })


    // useEffect(() => {
    //     Object.keys(schema.fields).map(value => {
    //         setDefaultValues(prev => ({ ...prev, [value]: '' }))
    //     })
    // }, [])// eslint-disable-line react-hooks/exhaustive-deps

    return <>
        {
            !isLoading
                ? <form onSubmit={ formik.handleSubmit }>
                    <div className="registration__modal-top">
                        <div className="registration__modal-tooltip">
                            {/* <img className="svg" src="/user.svg" alt="" /> */ }
                            <div className="registration__modal-tooltip-logo">
                                <Image
                                    src={ '/user.svg' }
                                    width='100%'
                                    height={ '100%' }
                                    alt='user'
                                />
                            </div>
                            <span>
                                Регистрация
                            </span>
                        </div>
                    </div>
                    <div className="registration__modal-body">
                        {/* <span className="error">
                            { errors['name'] }
                        </span> */}
                        <div className="registration__body-top">
                            <span>
                                Имя
                            </span>
                        </div>
                        <div className={ `registration__body-input login ${formik.errors['name'] && 'input__error'}` }>
                            <input
                                name={ 'name' }
                                value={ formik.values['name'] }
                                onChange={ formik.handleChange }
                                type="text"
                            />
                        </div>
                        {/* <span className="error">
                            { errors['lastname'] }
                        </span> */}
                        <div className="registration__body-top">
                            <span>
                                Фамилия
                            </span>
                        </div>
                        <div className={ `registration__body-input login ${formik.errors['lastname'] && 'input__error'}` }>
                            <input
                                name={ 'lastname' }
                                value={ formik.values['lastname'] }
                                onChange={ formik.handleChange }
                                type="text"
                            />
                        </div>
                        <span className="error">
                            { errors['email'] }
                        </span>
                        <div className="registration__body-top">
                            <span>
                                E-mail
                            </span>
                        </div>
                        <div className={ `registration__body-input login ${formik.errors['email'] && 'input__error'}` }>
                            <input
                                name={ 'email' }
                                value={ formik.values['email'] }
                                onChange={ formik.handleChange }
                                type="email"
                            />
                        </div>
                        <span className="error">
                            { errors['password'] }
                        </span>
                        <div className="registration__body-password">
                            <div className="registration__password-left">
                                <div className="registration__body-top">
                                    <span>
                                        Пароль
                                    </span>
                                </div>
                                <div className={ `registration__body-input login ${formik.errors['password'] && 'input__error'}` }>
                                    <input
                                        name={ 'password' }
                                        value={ formik.values['password'] }
                                        onChange={ formik.handleChange }
                                        type="password"
                                    />
                                </div>
                            </div>
                            <div className="regitration__password-right">
                                <div className="registration__body-top">
                                    <span>
                                        Повторите пароль
                                    </span>
                                </div>
                                <div className={ `registration__body-input login ${formik.errors['password_confirmation'] && 'input__error'}` }>
                                    <input
                                        name={ 'password_confirmation' }
                                        value={ formik.values['password_confirmation'] }
                                        onChange={ formik.handleChange }
                                        type="password"
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="registration__body-sign In">
                            <button onClick={ formik.handleSubmit }>Регистрация</button>
                        </div>
                        <div className="registration__body-sign In">
                            <span>У вас уже есть акаунт?</span>
                            <span onClick={ signIn }>Войти!</span>
                        </div>
                    </div>
                </form>
                : <ContentLoader />
        }
    </>
}