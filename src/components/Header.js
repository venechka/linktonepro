import { useAuth } from "hooks/useAuth";
import { HeaderContext } from "layouts/Common";
import Image from "next/image";
import { useRouter } from "next/router";
import { AppContext } from "pages/_app";
import { useContext, useEffect, useState } from "react";
import { ContentLoader } from "./ContentLoader";



export function Header() {

    const { isSideBarOpen, setIsSideBarOpen } = useContext(HeaderContext)

    const [userInputActive, setUserInputActive] = useState(false)
    const cliclOnUserInput = e => setUserInputActive(!userInputActive)

    const { app } = useContext(AppContext)

    const router = useRouter()

    const { logout } = useAuth()

    const logOut = () => logout()

    const openProile = () => router.push('/profile')

    const showSideBar = () => setIsSideBarOpen(!isSideBarOpen)

    const clickOnLogo = () => router.push('/')

    if (!app) return <></>

    return (
        <header className="header">
            <div className="header__left">
                <div
                    className="header__left-logo"
                    onClick={ clickOnLogo }
                >
                    <Image src={ '/logo.svg' } width='100%' height='35px' alt='logo' />
                </div>
                <div onClick={ showSideBar } className="header__burger">
                    <span></span>
                </div>
            </div>
            <div className="header__right">
                <div onClick={ cliclOnUserInput } className={ `header__right-user` }>
                    <div className={`header__user-input ${userInputActive && 'active'}`}>
                        <div className="header__input-wrapper">
                            <div className="header__user-avatar">
                                <Image
                                    layout="fill"
                                    loader={ () => app.user.picture ?? '/user.svg' }
                                    src={ app.user.picture ?? '/user.svg' }
                                    alt='avatar'
                                    objectFit="cover"
                                    objectPosition='cennter '
                                    unoptimized
                                />
                            </div>
                            <div className="header__user-initials">
                                <span>{ app.user.name } { app.user.lastname }</span>
                            </div>
                            <div className="header__user-arrowDown">
                                <Image src={ '/arrowDown.png' } width='100%' height='100%' alt='arrowDown' />
                            </div>
                        </div>  
                    </div>
                    <div className="header__user-dropDown">
                        <ul className="header__dropDown-items">
                            <li onClick={ openProile } className="header__dropDown-item">
                                <Image
                                    className="header__dropDown-item-icon"
                                    src={ '/profile.svg' }
                                    width='12px'
                                    height='12px'
                                    alt="profile"
                                />
                                <span >Профиль</span>
                            </li>
                            <li onClick={ logOut } className="header__dropDown-item">
                                <Image
                                    className="header__dropDown-item-icon"
                                    src={ '/off.svg' }
                                    width='12px'
                                    height='12px'
                                    alt="off"
                                />
                                <span >Выход</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    )
}