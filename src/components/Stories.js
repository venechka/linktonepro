import { $api } from 'api'
import Image from 'next/image'
import React from 'react'

// function Stories({ stories, removeStory }) {
function Stories({ stories, mutate, setStories, setDeletedStories }) {


    const removeStory = (story) => async () => {
        // await $api.delete(`/api/stories/${id}`)
        setStories(stories.filter(s => s.id !== story.id))
        if (+story.id) setDeletedStories(prev => [...prev, { id: story.id, deleted: true }])
        // setImgBlob(null)
        mutate()
    }


    return (
        <>
            {
                stories.length ? stories.map(story =>
                    <div key={ story.id } className="tabs__item-stories">
                        <span></span>
                        <div className={ `currentInfo__right-dropzone` }>
                            <div className="currentInfo__dropzone-img">
                                <video width="100%" height="100%" controls>
                                    <source src={ +story.id ? story.file : URL.createObjectURL(story.file) } />
                                </video>
                                <div
                                    className="currentInfo__dropzone-remove"
                                >
                                    {/* <img
                                        src="/remove.svg" alt=""
                                        onClick={ removeStory(story.id) } /> */}
                                    <Image
                                        src={ '/remove.svg' }
                                        width='100%'
                                        height={ '100%' }
                                        alt='delete'
                                        onClick={ removeStory(story) }
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                )
                    : null
            }
        </>
    )
}

export default Stories