import { $api, swrGetFetcher } from "api";
import { CurrentPageDropZone } from "components/CurrentPageDropZone";
import { CurrentPageMain } from "components/CurrentPageMain";
import Image from "next/image";
import { useRouter } from "next/router";
import { RELIZES } from "pathes/utils";
import { useContext, useEffect, useState } from "react";
import useSWR from "swr";
import { Button } from "ui/Button";
import { Input } from "ui/Input";
import Form from "./Form";
import * as Yup from 'yup'
import { useFormik } from "formik";
import { FormTabs } from "./FormTabs";
import FormTab from "./FormTab";
import { Card } from "./Card";
import { SearchInput } from "ui/Search";
import { ConfirmDeleteModal } from "./ConfirmDeleteModal";
import { AppContext } from "pages/_app";
import { v1 } from "uuid";
import { useTags } from "hooks/useTags";
import { useReleaseSchema } from "hooks/useReleaseSchema";
import { useRequest } from "hooks/useRequest";
import NotValidContent from "./NotValidContent";
import { CommonLayout } from "layouts/Common";
import { releaseSocialLinks } from "models/ReleaseSocial";
import VideoCard from "./VideoCard";
import Tags from "./Tags";
import LoadStories from "./LoadStories";
import Stories from "./Stories";
import { v4 } from "uuid";
import { appErros, MainFormContext } from "utils";
import { ContentLoader } from 'components/ContentLoader'


export function ReleaseFormPage({ update }) {

    const router = useRouter()
    const { release_id, id } = router.query

    const { data: artist } = useSWR(id ? `/api/artists/${id}` : null, swrGetFetcher)
    const { data: release, mutate: _mutate, error } = useSWR(release_id ? `/api/releases/${release_id}` : null, swrGetFetcher)
    if (error?.response?.status === 404) {
        router.push(`/artists/${id}/releases`)
    }

    const { app, mutate } = useContext(AppContext)

    const { schema } = useReleaseSchema(app ? app.dictionaries.release_types : [], artist ? artist.slug : '')

    const { set } = useRequest()

    const [unProcessableContent, setUnProcessableContent] = useState(null)

    const [searchInputValue, setSearchInputValue] = useState('')
    const setInputValue = e => setSearchInputValue(e.target.value)

    const [defaultValues, setDefaultValues] = useState(
        { name: '', slug: '', release_type_id: '', publish_date: { date: '', time: '12:00' }, label_company: '', tracks: '' }
    )


    const [videos, setVideos] = useState([])
    const { tags, addTags, addTag, removeTag, getTagsValue } = useTags()

    const [description, setDescription] = useState('')
    const setDescriptionValue = e => setDescription(e.target.value)

    const [stories, setStories] = useState([])
    const [deletedStories, setDeletedStories] = useState([])
    const [releateVideos, setReleateVideos] = useState([])

    const [isLoading, setIsLoading] = useState(false)

    useEffect(() => {

        if (update && release) {
            let {
                name,
                release_type_id,
                publish_date,
                label_company,
                tracks,
                related_videos: release_videos,
                tags: release_tags,
                social_links,
                description,
                cover,
                slug,
                stories: _stories,
                is_banned
            } = release


            if (is_banned) setUnProcessableContent('Этот релиз забанен')

            setStories(_stories)

            setVideos(release_videos.map(v => v.video))
            addTags(release_tags ? JSON.parse(release_tags).map(_tag => ({ id: v4(), title: _tag })) : [])

            setDescription(description ?? '')

            let parsedLinks = null
            try {
                if (JSON.parse(social_links)) {
                    parsedLinks = Object.keys(JSON.parse(social_links)).length
                        ? Object.keys(JSON.parse(social_links))
                            .reduce((acc, next) => ({ ...acc, [next]: { ...acc[next], value: JSON.parse(social_links)[next], error: false } }), {})
                        : socialLinksValue
                }
                else parsedLinks = socialLinksValue
            } catch (error) {
                parsedLinks = social_links
            }

            setSocialLinksValue(parsedLinks)

            let _tracks = null
            if (JSON.parse(tracks) && Array.isArray(JSON.parse(tracks))) _tracks = tracks ? JSON.parse(tracks).join('\n') : ''
            else _tracks = ''


            const [date, time] = publish_date.split(/\s/)



            setDefaultValues(() =>
            ({
                name,
                release_type_id,
                label_company,
                tracks: _tracks,
                publish_date: { date, time },
                slug
            }))
        }
    }, [release])// eslint-disable-line react-hooks/exhaustive-deps

    const [logoImg, setlogoImg] = useState('')

    const [socialLinksValue, setSocialLinksValue] = useState({
        yandex: { value: '', error: false },
        vk: { value: '', error: false },
        ok: { value: '', error: false },
        sber: { value: '', error: false },
        tiktok: { value: '', error: false },
        apple: { value: '', error: false },
        spotify: { value: '', error: false },
        youtube: { value: '', error: false },
        deezer: { value: '', error: false },
    })

    const regexp = /^(http|https):\/\/.+$/

    const setSocialLinksInputValue = field => e => {
        setSocialLinksValue(prev =>
        ({
            ...prev, [field]:
            {
                ...prev[field],
                value: e.target.value,
                error: e.target.value.length && !regexp.test(String(e.target.value).toLowerCase())
            }
        }))
        setUnProcessableContent(
            (e.target.value.length && !regexp.test(String(e.target.value).toLowerCase()))
                ? 'Ссылка должна содержать http(s)://'
                : null
        )
    }


    const [inputsValues, setInputsValues] = useState(null)
    const saveArtist = async data => {
        // console.log('st', stories.reduce((prev, next, index) => [...prev, { id: +next.id ? next.id : 'new', file_index: index }], []))
        // e.target.disabled = true
        const formData = new FormData()

        const { errors, ...other } = inputsValues

        // console.log('first', other['tracks'].match(/^.+/gm))
        // return

        for (const key in other) {
            if (!other[key] && !key.match(/(tracks)/g)) {
                setUnProcessableContent('Заполните все поля и загрузите аватар релиза')
                return
            }
        }

        if (!logoImg && !update) {
            setInputsErrors(prev => ({ ...prev, cover: true }))
            setUnProcessableContent('Загрузите обложку')
            return
        }

        if (inputsErrors['cover']) {
            return
        }

        for (const key in socialLinksValue) {
            if (socialLinksValue[key]['error']) {
                setUnProcessableContent('Неверный формат ссылок')
                return
            }
        }

        formData.append('social_links', JSON.stringify(Object.keys(socialLinksValue).reduce((acc, next) => ({
            ...acc, [next]: socialLinksValue[next]['value']
        }), {})))


        const { date, time } = other['publish_date']

        let storiesArr = []
        if (stories.length) {
            const s = stories.filter(s => !+s.id)
            storiesArr.push(...s.reduce((prev, next, index) => [...prev, { id: 'new', file_index: index }], []))
            storiesArr.forEach((item, index) => formData.append('stories_files[]', s[index].file))
        }

        if (deletedStories.length) {
            storiesArr.push(...deletedStories.reduce((prev, next) => [...prev, { id: next.id, delete: true }], []))
        }

        if (storiesArr.length && update) {
            formData.append('stories', JSON.stringify(storiesArr))
        }

        formData.append('name', other['name'])
        formData.append('release_type_id', other['release_type_id'])
        formData.append('label_company', other['label_company'])
        formData.append('artist_id', artist.id)
        formData.append('publish_date', `${date} ${time}`)
        formData.append('description', description)
        formData.append('tags', JSON.stringify(getTagsValue()))
        other['tracks'].match(/^.+/gm) && formData.append('tracks', JSON.stringify(other['tracks'].match(/^.+/gm)))
        formData.append('slug', other['slug'])
        formData.append('related_videos', JSON.stringify([...videos.map(v => v.id)]))
        if (logoImg) formData.append('cover', logoImg)

        setIsLoading(true)
        if (update) {
            formData.append('_method', 'PUT')
            const error = await set(`/api/releases/${release_id}`, formData, `/artists/${id}/releases`)
            // if (error) setUnProcessableContent(appErros['ru'][error])
            if (error) {
                setIsLoading(false)
                setUnProcessableContent(appErros['ru'][error])
                return
            }
            setIsLoading(false)
            mutate()
            return
        }
        const error = await set(`/api/releases`, formData, `/artists/${id}/releases`)
        setIsLoading(false)
        // if (error) setUnProcessableContent(appErros['ru'][error])
        if (error) {
            setIsLoading(false)
            setUnProcessableContent(appErros['ru'][error])
            return
        }
        mutate()
    }

    const [inputsErrors, setInputsErrors] = useState({ cover: false })

    const [isDelete, setIsDelete] = useState(false)
    const removeRelease = async () => {
        await $api.delete(`/api/releases/${release_id}`)
        mutate()
        router.push(`/artists/${id}/releases`)
    }

    const cancel = () => {
        if (isDelete) {
            setIsDelete(false)
            return
        }
        router.push(`/artists/${id}/releases`)
    }

    const [artistVideos, setArtistVideos] = useState(false)

    const showOrHide = () => setArtistVideos(!artistVideos)

    const tieVideo = item => async () => {
        // setReleateVideos(prev => [...prev, item.id])
        // if (id) {
        //     const obj = {
        //         '_method': 'PUT',
        //         'release_id': release_id
        //     }
        //     await $api.post(`/api/videos/${id}`, obj)
        setVideos(prev => [...prev, item])
        setArtistVideos(false)

        //     return
        // }
    }

    const unTieVideo = id => async () => {
        // await $api.post(`/api/videos/${id}`, {
        //     _method: 'PUT',
        //     release_id: null
        // })
        // setReleateVideos(prev => prev.filter(videoId => videoId !== id))
        setVideos(prev => prev.filter(video => video.id !== id))

    }

    const filterTieVidoes = item => String(item.name.toLowerCase()).includes(searchInputValue.toLowerCase())

    const addTagByEnter = e => {

        if (e.code === 'Enter' || e.code === 'Space') {
            addTag(e)
        }
    }

    return (
        <CommonLayout
            contentHeaderTitle={ 'Редактирование релиза' }
            isUpdate={ update && { link: `${process.env.NEXT_PUBLIC_APP_URL}/${artist?.slug}/${release?.slug}` } }
            crumbs={ [
                {
                    id: 0, title: artist ? artist.name : 'Артист'
                },
                {
                    id: 1, title: release ? release.name : 'Релиз'
                },
            ] }
            title={ `Редактирование релиза` }
        >

            <>
                {
                    unProcessableContent && <NotValidContent error={ unProcessableContent } />
                }
                <div className="currentInfo">
                    <div className="currentInfo__wrapper">
                        <div className="currentInfo__inner">
                            <MainFormContext.Provider value={ { setInputsValues } }>
                                <CurrentPageMain
                                    esenceTitle='Добавьте ключевую информацию о релизе'
                                    page={ RELIZES }
                                    schema={ schema }
                                    defaultValues={ defaultValues }
                                />
                            </MainFormContext.Provider>
                            <CurrentPageDropZone
                                title={ 'Обложка' }
                                text='Загрузите обложку релиза 1:1.В формате JPG или PNG (max 10Mb)'
                                setImg={ setlogoImg }
                                error={ inputsErrors['cover'] }
                                img={ release ? release.cover : '' }
                                setError={ setInputsErrors }
                                required
                                setMessage={ setUnProcessableContent }
                                dropZoneId='cover'
                            />
                            <div className="currentInfo__tabs">
                                <FormTabs >
                                    <FormTab label={ 'Ссылки на площадки' }>
                                        {
                                            releaseSocialLinks.map(link =>
                                                <Input
                                                    key={ link.id }
                                                    labelTxt={ link.labelTxt }
                                                    withIcon
                                                    withFlag
                                                    flag={ link?.flag }
                                                    icon={ link.icon }
                                                    value={ socialLinksValue[link.type]['value'] }
                                                    onChange={ setSocialLinksInputValue(link.type) }
                                                    iconBg={ 'gray' }
                                                    error={ socialLinksValue[link.type]['error'] }
                                                />
                                            )
                                        }

                                    </FormTab>
                                    {
                                        update && <FormTab label={ 'Связанные видео' }>
                                            {
                                                artistVideos
                                                    ? <>
                                                        <div className="artist__videos-search">
                                                            {/* <span onClick={ hideArtistVideos }>Назад</span> */ }
                                                            <span onClick={ showOrHide }>Назад</span>
                                                            <SearchInput
                                                                placeholder="Найдите видео"
                                                                value={ searchInputValue }
                                                                onChange={ setInputValue }
                                                            />
                                                        </div>
                                                        <div className="artist__videos">
                                                            {
                                                                artist?.videos.length ? artist.videos
                                                                    .filter(filterTieVidoes)
                                                                    .map(video =>
                                                                        <VideoCard
                                                                            key={ video.id }
                                                                            video={ video }
                                                                            unTieVideo={ unTieVideo }
                                                                            app={ app }
                                                                            videos={ videos }
                                                                        >
                                                                            <Button
                                                                                color="primary"
                                                                                txt={ 'Привязать' }
                                                                                onClick={ tieVideo(video) }
                                                                            />
                                                                        </VideoCard>
                                                                    )
                                                                    : <h1 className="empty">У вас нет видео</h1>
                                                            }
                                                        </div>
                                                    </>
                                                    :
                                                    <div className="tabs__dropzone-stories">
                                                        {
                                                            videos && videos.map(video =>
                                                                <VideoCard
                                                                    key={ video.id }
                                                                    video={ video }
                                                                    unTieVideo={ unTieVideo }
                                                                    app={ app }
                                                                >
                                                                    <Button
                                                                        color="primary"
                                                                        txt={ 'Отвязать' }
                                                                        onClick={ unTieVideo(video.id) }
                                                                    />
                                                                </VideoCard>
                                                            )
                                                        }
                                                        {/* <div onClick={ showArtistVideos } className="tabs__dropzone-card tie"> */ }
                                                        <div onClick={ showOrHide } className="tabs__dropzone-card tie">
                                                            +
                                                        </div>
                                                    </div >
                                            }
                                        </FormTab>
                                    }
                                    <FormTab label={ 'Промо' }>
                                        <Tags
                                            tags={ tags }
                                            addTag={ addTag }
                                            removeTag={ removeTag }
                                            addTagByEnter={ addTagByEnter }
                                        />

                                        <div className="tabs__item-caption">
                                            <span>Описание <br />(max 500 символов)</span>
                                            <textarea
                                                value={ description }
                                                onChange={ setDescriptionValue }
                                                maxLength={ 500 }
                                            ></textarea>
                                        </div>
                                        {
                                            <LoadStories
                                                stories={ stories }
                                                setStories={ setStories }
                                                mutate={ _mutate }
                                                essence='release_id'
                                                essence_id={ release?.id }
                                                setUnProcessableContent={ setUnProcessableContent }
                                            />
                                        }
                                        <Stories
                                            stories={ stories }
                                            setStories={ setStories }
                                            setDeletedStories={ setDeletedStories }
                                            mutate={ _mutate }
                                        />
                                    </FormTab>
                                </FormTabs>
                            </div>
                            <div className="currentInfo__btns">
                                <div className="currentInfo__btns-left">
                                    {/* <Button onClick={ formik.handleSubmit } txt='Сохранить' color='primary' /> */ }
                                    {
                                        !isLoading
                                            ? <Button onClick={ saveArtist } txt='Сохранить' color='primary' />
                                            : <ContentLoader />
                                    }
                                    <Button onClick={ cancel } txt='Отменить' color='cancel' />
                                </div>
                                {
                                    update && <Button onClick={ () => setIsDelete(!isDelete) } txt='Удалить релиз' color='delete' />
                                }

                            </div>
                            {
                                isDelete && <ConfirmDeleteModal esence={ 'релиз' }>
                                    <Button onClick={ removeRelease } txt='Удалить релиз' color='delete' />
                                    <Button onClick={ cancel } txt='Отменить' color='cancel' />
                                </ConfirmDeleteModal>
                            }
                        </div>
                    </div>
                </div >
            </>
        </CommonLayout>
    )
}