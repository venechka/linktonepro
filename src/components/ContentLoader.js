import { Oval } from 'react-loader-spinner'

export function ContentLoader() {
    return (
        <div className="loader">
            <Oval
                ariaLabel="loading-indicator"
                height={ 50 }
                width={ 50 }
                strokeWidth={ 2 }
                color="#00a1f2"
                secondaryColor="white"
            />
        </div>
    )
}