import Image from 'next/image'


export function Card({ title = 'Слот', img = '/artist1.jpg', type = 'Группа', format }) {
    return (
        <div
            className="_card"
        >
            <div className="_card__inner">
                <div className={ `_card__img ${format && 'video'}` }
                    style={
                        {
                            backgroundImage: `url(${img})`
                        } }
                >
                </div>
                <div className="_card-body">
                    <span className="_card-type">
                        {
                            type
                        }
                    </span>
                    <span className="_card-title">
                        { title }
                    </span>
                </div>
            </div>
        </div>
    )
}