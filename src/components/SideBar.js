import Image from "next/image";
import { createContext } from "react/cjs/react.production.min";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
import { AppContext } from "pages/_app";
import { HeaderContext } from "layouts/Common";



export function SideBar() {

    const router = useRouter()

    const { isSideBarOpen } = useContext(HeaderContext)
    useEffect(() => {
        setSelectedItem(Number(router.query.id))
        setSelectedInnerItem(router.asPath.split('/')[3])
        // document.querySelector('body').style.overflowY = 'scroll'
    }, [router])

    const [selectedItem, setSelectedItem] = useState(null)
    const [selectedInnerItem, setSelectedInnerItem] = useState(null)
    const [isClamped, setIsClamped] = useState(false)

    const { app } = useContext(AppContext)

    const clickOnBurger = () => {
        setIsClamped(!isClamped)
        setSelectedItem(null)
    }

    const selectItem = index => () => {
        setSelectedInnerItem(null)
        setIsClamped(false)
        if (selectedItem === index) {
            setSelectedItem(null)
            return
        }
        setSelectedItem(index)
    }

    return (
        <div className={ `sideBar ${isClamped && 'clamped'} ${isSideBarOpen && 'show'}` }>
            <div className="sideBar__top">
                <div className="sideBar__top-left">
                    <span>Навигация</span>
                </div>
                <div onClick={ clickOnBurger } className="sideBar__top-right">
                    <div className="sideBar__top-right-wrapper">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
            <div className="sideBar__bottom">
                <ul className="sideBar__bottom-items">
                    <Link href={ '/' } passHref>
                        <li className="sideBar__item" datatype="0">
                            <div className="sideBar__item-top">
                                <div className="sideBar__item-left">
                                    <Image className="sideBar__item-icon" src={ '/dashboard.svg' } width='18px' height='18px' alt="dashboard" />
                                    <span className="sideBar__item-top-title">Артисты</span>
                                </div>
                            </div>
                        </li>
                    </Link>
                    {
                        app && app.user.artists.map((_item, index) =>
                            <li
                                key={ _item.id }
                                onClick={ selectItem(_item.id) }
                                className={ `sideBar__item ${selectedItem === _item.id && 'opened'}` }
                            >
                                <div className="sideBar__item-top">
                                    <div className="sideBar__item-left">
                                        <Image className="sideBar__item-icon" src={ '/music.svg' } width='18px' height='18px' alt="music" />
                                        <span className="sideBar__item-top-title">{ _item.name }</span>
                                    </div>
                                    <div className="sideBar__item-arrowDown">
                                        <Image src={ '/arrowDown.svg' } width='14px' height='14px' alt="arrowDown" />
                                    </div>
                                </div>
                                <ul className="sideBar__item-inner">
                                    {
                                        [
                                            { id: 0, title: 'Профиль', endPoint: `/artists/${_item.id}/profile`, path: 'profile' },
                                            { id: 1, title: 'Релизы', endPoint: `/artists/${_item.id}/releases`, path: 'releases' },
                                            { id: 2, title: 'Видео', endPoint: `/artists/${_item.id}/videos`, path: 'videos' },
                                            { id: 3, title: 'Концерты', endPoint: `/artists/${_item.id}/concerts`, path: 'concerts' },
                                            { id: 4, title: 'Настройки', endPoint: `/artists/${_item.id}/settings`, path: 'settings' },
                                        ].map(_item =>
                                            <Link key={ _item.id }
                                                href={ _item.endPoint }
                                                passHref
                                            >
                                                <li
                                                    key={ _item.id }
                                                    data-type={ _item.path }
                                                    className={ `sideBar__item-inner-item ${selectedInnerItem === _item.path && 'active'}` }
                                                >{ _item.title }</li>
                                            </Link>
                                        )
                                    }
                                </ul>
                            </li>
                        )
                    }
                </ul>
            </div>
        </div>
    )
}