import React from 'react'

function Tags({ tags, removeTag, addTag, addTagByEnter }) {
    return (
        <div className="tabs__item-tags">
            <span>Теги</span>
            <div className="tabs__tags-input">
                {
                    tags && tags.map(_item =>
                        <span key={ _item.id } className="tabs__item-tag">
                            { _item.title }
                            <span
                                data-id={ _item.id }
                                onClick={ removeTag(_item.id) }
                            ></span>
                        </span>
                    )
                }
                {
                    tags.length === 5
                        ? null
                        : <input
                            onBlur={ addTag }
                            onKeyUp={ addTagByEnter }
                            maxLength={ 25 }
                            type="text"
                        />
                }

            </div>
        </div>
    )
}

export default Tags