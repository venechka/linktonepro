import Image from "next/image"
import Form from "./Form"

export function CurrentPageMain(config) {
    const {
        esenceTitle,
        page,
        inputs,
        children,
        schema,
        defaultValues
    } = config

    // console.log('current inputs',defaultValues);

    return (
        <div className="currentInfo__main">
            <div className="currentInfo__main-left">
                <div className="currentInfo__left-icon">
                    <Image src={ '/box.svg' } width='83px' height='83px' alt="box" />
                </div>
                <h2 className="currentInfo__left-title">Основная информация</h2>
                {/* <p className="currentInfo__left-text">Добавьте ключевую информацию о { esenceTitle }.</p> */ }
                <p className="currentInfo__left-text">{ esenceTitle }.</p>
            </div>
            <div className="currentInfo__main-right">
                <ul className="currentInfo__right-inputs">
                    {/* {
                        children
                    } */}
                    <Form
                        schema={ schema }
                        defaultValues={defaultValues}
                    />
                </ul>
            </div>
        </div>
    )

}