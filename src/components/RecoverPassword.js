import { $api } from "api"
import Image from "next/image"
import { useRouter } from "next/router"
import { useState } from "react"
import { appErros } from "utils"

export function RecoverPassword() {

    const router = useRouter()

    const signIn = () => router.push('/login')

    const [email, setEmail] = useState('')
    const [status, setStatus] = useState('')

    const setValue = e => setEmail(e.target.value)

    const regexp = /\d\w/gi

    const sendEmail = async () => {
        // if (!regexp.test(email)) return
        try {
            const { data } = await $api.post(`/forgot-password`, { email })
            setStatus(appErros['ru'][data.status])
            setTimeout(() => {
                router.push('/login')
            }, 1000);
        } catch (error) {
            setStatus(appErros['ru'][error.response.data.message])
        }
    }

    return <div className="registration">
        <div className="registration__inner">
            <div className="registration__modal">
                <div className="registration__modal-logo">
                    {/* <img src={ '/logo.png' } alt="" /> */ }
                    <Image
                        layout="fill"
                        src={ '/logo.svg' }
                        alt='logo'
                    />
                </div>
                <div className="registration__modal-top">
                    <div className="registration__modal-tooltip">
                        {/* <img className="svg" src="/user.svg" alt="" />  */ }
                        <div className="registration__modal-tooltip-logo">
                            <Image
                                width={ '100%' }
                                height='100%'
                                src={ '/user.svg' }
                                alt='user'
                            />
                        </div>
                        <span>Восстановление пароля</span>
                    </div>
                </div>
                <div className="registration__modal-body">
                    {
                        status && <span className="notification error">{ status }</span>
                    }
                    <div className="registration__body-note">
                        <span>Введите ваш e-mail и мы отправим вам инструкции по восстановлению пароля.</span>
                    </div>
                    {/* <div className="registration__body-top">
                        <span>
                            E-mail
                        </span>
                    </div>
                    <div className="registration__body-input">
                        <input
                            value={ email }
                            onChange={ setValue }
                            placeholder="E-MAIL"
                            type="email"
                        />
                        <button>Отправить</button>
                    </div> */}
                    <div className="registration__body-top">
                        <span>
                            E-mail
                        </span>
                    </div>
                    <div className={ `registration__body-input login ` }>
                        <input
                            name={ 'email' }
                            value={ email }
                            onChange={ e => setEmail(e.target.value) }
                            type="text"
                        />
                        <div className="registration__input-icon">
                            {/* <img src="/email.svg" alt="" /> */ }
                            <Image
                                // layout="fill"
                                src={ '/email.svg' }
                                alt='email'
                                width={ '100%' }
                                height='100%'
                            />
                        </div>
                    </div>
                    <div className="registration__body-sign In">
                        <button onClick={ sendEmail }>Отправить</button>
                    </div>
                    <div className="registration__body-remembered">
                        <span>Вспомнили пароль? </span>
                        <span onClick={ signIn }>Войти!</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
}