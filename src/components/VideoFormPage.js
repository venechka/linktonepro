import { $api, swrGetFetcher } from "api";
import { CurrentPageMain } from "components/CurrentPageMain";
import Image from "next/image";
import { useRouter } from "next/router";
import { VIDEOS } from "pathes/utils";
import { useContext, useEffect, useState } from "react";
import { Button } from "ui/Button";
import Form from "./Form";
import * as Yup from 'yup'
import { useFormik } from "formik";
import { FormTabs } from "./FormTabs";
import FormTab from "./FormTab";
import { Card } from "./Card";
import { useLoading } from "hooks/useLoading";
import { ContentLoader } from "./ContentLoader";
import { ConfirmDeleteModal } from "./ConfirmDeleteModal";
import { AppContext } from "pages/_app";
import { useTags } from "hooks/useTags";
import { useRequest } from "hooks/useRequest";
import NotValidContent from "./NotValidContent";
import useSWR from "swr";
import { CommonLayout } from "layouts/Common";
import Tags from "./Tags";
import LoadStories from "./LoadStories";
import Stories from "./Stories";
import { v4 } from "uuid";
import { appErros, MainFormContext } from "utils";



export function VideoFormPage({ update }) {

    const router = useRouter()
    const { video_id, id } = router.query

    const { data: artist } = useSWR(id ? `/api/artists/${id}` : null, swrGetFetcher)
    const { data: video, mutate: _mutate, error } = useSWR(video_id ? `/api/videos/${video_id}` : null, swrGetFetcher)
    if (error?.response?.status === 404) {
        router.push(`/artists/${id}/videos`)
    }

    const { app, mutate } = useContext(AppContext)

    const [description, setDescription] = useState('')
    const setDescriptionValue = e => setDescription(e.target.value)


    const { set } = useRequest()

    const [unProcessableContent, setUnProcessableContent] = useState(null)

    const { tags, addTags, addTag, removeTag, getTagsValue } = useTags()

    const [stories, setStories] = useState([])
    const [deletedStories, setDeletedStories] = useState([])

    // const { isLoading } = useLoading(video)
    const [isLoading, setIsLoading] = useState(false)

    const regexp = /^(http|https):\/\/.+$/

    const schema = Yup.object().shape({
        name: Yup.string()
            .required('Введите название видео')
            .label('Название видео'),
        slug: Yup.string()
            .meta({
                inputAddress: true,
                artist_slug: artist ? artist.slug : ''
            })
            .test('onluEng', 'Некорректные символы', (value) => {
                const regex = /^[a-zA-Z0-9\-]{3,}$/
                return regex.test(String(value).toLowerCase())
            })
            .label('Адрес видео')
            .required('Укажите адрес видео')
        ,
        youtubeLink: Yup.string()
            .required('Введите ссылку на видео')
            .meta({
                youtubeLink: true
            })
            .label('Ссылка на видео')
            .test('correct__link', 'Вы ввели не коректную ссылку', async (data) => {
                try {
                    if (regexp.test(String(data).toLowerCase())) return true
                } catch (error) {
                    return false
                }
            }),
        type: Yup.string()
            .required('Выберите тип видео')
            .meta({
                select: true,
                dropDownItems: app ? app.dictionaries.video_types : []
            })
            .label('Тип видео'),
        publish_date: Yup.object()
            .meta({
                dateTime: true,
            })
            .label('Дата и время видео')
            .shape({
                date: Yup.string()
                    .required('Введите дату'),
                time: Yup.string(),
            }),
    })


    const [defaultValues, setDefaultValues] = useState(
        { name: '', slug: '', youtubeLink: '', type: '', publish_date: { date: '', time: '12:00' } }
    )

    useEffect(() => {
        if (update && video) {
            const {
                name,
                link,
                video_type_id,
                publish_date,
                description,
                tags,
                slug,
                stories: _stories,
                is_banned
            } = video

            if (is_banned) setUnProcessableContent('Это видео заблокировано')

            const [date, time] = publish_date.split(/\s/g)

            setStories(_stories)

            setDefaultValues({
                name: name,
                youtubeLink: link,
                type: video_type_id,
                publish_date: { date, time },
                slug
            })

            const parsedTags = tags ? JSON.parse(tags) : []
            setDescription(description ?? '')
            addTags(parsedTags.map(_tag => ({ id: v4(), title: _tag })))
        }
    }, [video])// eslint-disable-line react-hooks/exhaustive-deps



    const [inputsValues, setInputsValues] = useState(null)
    const saveVideo = async data => {


        const { errors, ...other } = inputsValues

        for (const key in other) {
            if (!other[key]) {
                setUnProcessableContent('Заполните все поля')
                return
            }
        }

        const formData = new FormData()

        const { date, time } = other['publish_date']

        let storiesArr = []
        if (stories.length) {
            const s = stories.filter(s => !+s.id)
            storiesArr.push(...s.reduce((prev, next, index) => [...prev, { id: 'new', file_index: index }], []))
            storiesArr.forEach((item, index) => formData.append('stories_files[]', s[index].file))
        }

        if (deletedStories.length) {
            storiesArr.push(...deletedStories.reduce((prev, next) => [...prev, { id: next.id, delete: true }], []))
        }

        if (storiesArr.length && update) {
            formData.append('stories', JSON.stringify(storiesArr))
        }

        formData.append('name', other['name'])
        formData.append('link', other['youtubeLink'])
        formData.append('artist_id', id)
        formData.append('tags', JSON.stringify(getTagsValue()))
        formData.append('video_type_id', other['type'])
        formData.append('publish_date', `${date} ${time}`)
        formData.append('description', description)
        formData.append('slug', other['slug'])

        setIsLoading(true)
        if (update) {
            formData.append('_method', 'PUT')
            const error = await set(`/api/videos/${video_id}`, formData, `/artists/${id}/videos`)
            setIsLoading(false)
            // if (error) setUnProcessableContent(appErros['ru'][error])
            if (error) {
                setIsLoading(false)
                setUnProcessableContent(appErros['ru'][error])
            }

            mutate()
            return
        }
        const error = await set(`/api/videos`, formData, `/artists/${id}/videos`)
        setIsLoading(false)
        // if (error) setUnProcessableContent(appErros['ru'][error])
        if (error) {
            setIsLoading(false)
            setUnProcessableContent(appErros['ru'][error])
        }
        mutate()
    }


    const [isDelete, setIsDelete] = useState(false)
    const removeVideo = async () => {
        await $api.delete(`/api/videos/${video_id}`)
        mutate()
        router.push(`/artists/${id}/videos`)
    }

    const cancel = () => {
        if (isDelete) {
            setIsDelete(false)
            return
        }
        router.push(`/artists/${id}/videos`)
    }

    const addTagByEnter = e => {

        if (e.code === 'Enter' || e.code === 'Space') {
            addTag(e)
        }
    }

    return (
        <CommonLayout
            contentHeaderTitle={ 'Редактирование видео' }
            isUpdate={ update && { link: `${process.env.NEXT_PUBLIC_APP_URL}/${artist?.slug}/${video?.slug}` } }
            crumbs={ [
                { id: 0, title: artist ? artist.name : 'Артист' },
                { id: 1, title: video ? video.name : 'Видео' },
            ] }
            title={ `Редактирование видео` }
        >

            <>
                {
                    unProcessableContent && <NotValidContent error={ unProcessableContent } />
                }
                <div className="currentInfo">
                    <div className="currentInfo__wrapper">
                        <div className="currentInfo__inner">
                            <MainFormContext.Provider value={ { setInputsValues } }>
                                <CurrentPageMain
                                    esenceTitle='Добавьте ключевую информацию о видео'
                                    page={ VIDEOS }
                                    defaultValues={ defaultValues }
                                    schema={ schema }
                                />
                            </MainFormContext.Provider>
                            <div className="currentInfo__tabs">
                                <FormTabs>
                                    {
                                        video ?
                                            video.release && <FormTab label={ 'Связанный релиз' }>
                                                <li className="mainInfo__content-card">
                                                    <Card
                                                        title={ video.release?.name }
                                                        img={ video.release?.cover }
                                                    />
                                                </li>
                                            </FormTab>
                                            : null
                                    }

                                    <FormTab label={ 'Промо' }>
                                        {
                                            <Tags
                                                tags={ tags }
                                                addTag={ addTag }
                                                removeTag={ removeTag }
                                                addTagByEnter={ addTagByEnter }
                                            />
                                        }

                                        <div className="tabs__item-caption">
                                            <span>Описание <br />(max 500 символов)</span>
                                            <textarea
                                                value={ description }
                                                onChange={ setDescriptionValue }
                                                maxLength={ 500 }
                                            ></textarea>
                                        </div>
                                        {

                                            <LoadStories
                                                stories={ stories }
                                                setStories={ setStories }
                                                mutate={ _mutate }
                                                essence='video_id'
                                                essence_id={ video?.id }
                                                setUnProcessableContent={ setUnProcessableContent }
                                            />
                                        }
                                        <Stories
                                            stories={ stories }
                                            mutate={ _mutate }
                                            setStories={ setStories }
                                            setDeletedStories={ setDeletedStories }
                                        />
                                    </FormTab>
                                </FormTabs>
                            </div>
                            <div className="currentInfo__btns">
                                <div className="currentInfo__btns-left">
                                    {/* <Button onClick={ formik.handleSubmit } txt='Сохранить' color='primary' /> */ }
                                    {
                                        !isLoading
                                            ? <Button onClick={ saveVideo } txt='Сохранить' color='primary' />
                                            : <ContentLoader />
                                    }
                                    <Button onClick={ cancel } txt='Отменить' color='cancel' />
                                </div>
                                {
                                    update && <Button onClick={ () => setIsDelete(!isDelete) } txt='Удалить видео' color='delete' />
                                }

                            </div>
                        </div>
                        {
                            isDelete && <ConfirmDeleteModal esence={ 'видео' }>
                                <Button onClick={ removeVideo } txt='Удалить видео' color='delete' />
                                <Button onClick={ cancel } txt='Отменить' color='cancel' />
                            </ConfirmDeleteModal>
                        }
                    </div>
                </div>
            </>
        </CommonLayout >
    )
}
