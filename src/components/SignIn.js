import { useAuth } from "hooks/useAuth"
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import { ContentLoader } from "./ContentLoader"
import * as Yup from 'yup'
import { useFormik } from "formik"
import { $api } from "api"
import Image from "next/image"
import { appErros } from "utils"



export function SignIn() {

    const router = useRouter()

    const [errors, setErrors] = useState({
        email: '',
        status: null
    })

    const { login, mutate, status, verifyEmail } = useAuth({
        middleware: 'guest',
        redirectIfAuthenticated: '/',
    })


    const signUp = () => router.push('/sign-up')
    const recoverPasswrod = () => router.push('/recover-password')


    // const [status, setStatus] = useState(null)


    const [inputErrors, setInputErrors] = useState({
        email: '',
        password: '',
        status: null
    })

    const [isLoading, setIsLoading] = useState(false)

    const signIn = async data => {
        setIsLoading(true)
        await login({ setErrors, ...data })
        await mutate()
        setIsLoading(false)
    }

    const schema = Yup.object().shape({
        email: Yup.string()
            .required('Введите email')
            .matches(/^([\w/i\-\.\d/g]{1,})@([\w/i/g]{2,}\.[\w/i/g]{2,5})$/),
        password: Yup.string()
            .required('Введите пароль')
            .matches(/^([\/\.\(\)\{\}\[\]\wi\d]){8,}$/),
    })

    const formik = useFormik({
        validationSchema: schema,
        initialValues: { email: '', password: '' },
        enableReinitialize: true,
        onSubmit: signIn
    })

    useEffect(() => {
        if (!router.isReady) return
        if (router.query.emailVerificationLink) {
            verifyEmail(router.query.emailVerificationLink, setErrors)
        }
    }, [router.query])// eslint-disable-line react-hooks/exhaustive-deps


    useEffect(() => {
        if (status && status['code'] === 409) {
            setErrors({ email: appErros['ru'][status['error']], status: status['code'] })
        }
    }, [status])



    const sendEmail = async () => {
        const { data, status } = await $api.post(`/email/verification-notification`)

        if (status !== 200) {
            setErrors({ email: data.status })
            return
        }
        setErrors({ email: appErros['ru'][data.status] })
    }

    return <>
        {
            !isLoading
                ? <form onSubmit={ formik.handleSubmit }>
                    <div className="registration__modal-top">
                        <div className="registration__modal-tooltip">
                            {/* <img className="svg" src="/user.svg" alt="" /> */ }
                            <div className="registration__modal-tooltip-logo">
                                <Image
                                    src={ '/user.svg' }
                                    width='100%'
                                    height={ '100%' }
                                    alt='user'
                                // className="svg"
                                />
                            </div>
                            <span>
                                Вход
                            </span>
                        </div>
                    </div>
                    <div className="registration__modal-body">
                        <span className="error">
                            { errors['email'] }
                            { errors?.status === 409 && <span
                                className="submit-email"
                                onClick={ sendEmail }>Отправьте повторно</span>
                            }
                        </span>
                        <div className="registration__body-top">
                            <span>
                                E-mail
                            </span>
                        </div>
                        <div className={ `registration__body-input login ${formik.errors['email'] && 'input__error'}` }>
                            <input
                                name={ 'email' }
                                value={ formik.values['email'] }
                                onChange={ formik.handleChange }
                                type="text"
                            />
                            <div className="registration__input-icon">
                                {/* <img src="/email.svg" alt="" /> */ }
                                <Image
                                    src={ '/email.svg' }
                                    width='100%'
                                    height={ '100%' }
                                    alt='email'
                                />
                            </div>
                        </div>
                        <span className="error">
                            { inputErrors['password'] }
                        </span>
                        <div className={ `registration__body-top password` }>
                            <span>
                                Пароль
                            </span>
                            <span onClick={ recoverPasswrod }>
                                Забыли пароль?
                            </span>
                        </div>
                        <div className={ `registration__body-input password ${formik.errors['password'] && 'input__error'}` }>
                            <input
                                name={ 'password' }
                                value={ formik.values['password'] }
                                onChange={ formik.handleChange }
                                type="password"
                            />
                            <div className="registration__input-icon">
                                {/* <img src="/lock.svg" alt="" /> */ }
                                <Image
                                    src={ '/lock.svg' }
                                    width='100%'
                                    height={ '100%' }
                                    alt='email'
                                />
                            </div>
                        </div>
                        <div className="registration__body-sign-In">
                            {/* <label>
                    <input type="checkbox" />
                    Remember Me
                </label> */}
                            {/* <button onClick={ signIn }>Sign In</button> */ }
                            <button onClick={ formik.handleSubmit }>Войти</button>
                        </div>
                        <div className="registration__body-sign Up">
                            <span>У вас еще нет аккаунта?</span>
                            <span onClick={ signUp }>Регистрация!</span>
                        </div>
                    </div>
                </form>
                : <ContentLoader />
        }
    </>
}