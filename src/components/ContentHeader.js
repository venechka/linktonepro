import Image from 'next/image'
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'
import { Fragment } from 'react/cjs/react.production.min'

export function ContentHeader({ title = 'Артисты', crumbs = [], isUpdate = false }) {

    const router = useRouter()
    const routeName = '/artists/[id]/profile'


    return (
        <header className="contentHeader">
            <div className="contentHeader__inner">
                <div className="contentHeader__left">
                    <h2 className="contentHeader__left-title">{ title }</h2>
                    {
                        isUpdate && <a href={`${isUpdate['link']}`} className="contentHeader__left-link" target={ '_blank' } rel='noreferrer'>
                            <Image src={ '/eye.svg' } width='20px' height='20px' alt='eye'/>
                        </a>
                    }

                </div>
                <div className="contentHeader__right">
                    <div className="contentHeader__right-wrapper">
                        <Image className='contentHeader__right-img' src={ '/home.svg' } width='100%' height='100%' alt='home' />
                    </div>
                    <span>Главная</span>{ crumbs && crumbs.map(_item =>
                        <Fragment key={ _item.id }>
                            <span>{ _item.title }</span>
                        </Fragment>
                    ) }
                </div>
            </div>
        </header>
    )
}