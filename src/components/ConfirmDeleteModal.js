export function ConfirmDeleteModal({ children, esence }) {
    return (
        <div className="remove">
            <div className="remove__inner">
                <div className="remove__modal">
                    <h3 className="remove__modal-title">Вы точно хотите удалить { esence }?</h3>
                    <div className="remove__modal-btns">
                        {
                            children
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}