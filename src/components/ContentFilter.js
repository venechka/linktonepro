import Image from 'next/image'

export function ContentFilter(config) {

    const { title = 'Типы артистов',
        bodyItems = [{ id: 0, title: 'Группы' }],
        filterArr,
        setArr,
        esence
    } = config

    const clickOnTop = (e) => e.target.classList.toggle('closed')
    const filter = (item) => () => setArr(item)

    return <div className="contentFilter">
        <div className="contentFilter__inner">
            <div onClick={ clickOnTop } className="contentFilter__top closed">
                <div className="contentFilter__top-wrapper">
                    <span>{ title }</span>
                    <div className="contentFilter__top-arrowDown">
                        <div className="contentFilter__top-arrowDown-wrapper">
                            <Image
                                src={ '/arrowDown.png' }
                                width='100%'
                                height='100%'
                                alt='arrowDown'
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className="contentFilter__body">
                <ul className="contentFilter__body-items">
                    <li onClick={ filter(0) } className="contentFilter__body-item">
                        <span>
                            Все
                        </span>
                        <span>
                            { esence?.length ?? 0 }
                        </span>
                    </li>
                    {
                        bodyItems.map(_item =>
                            <li onClick={ filter(_item.id) } key={ _item.id } className="contentFilter__body-item">
                                <span>
                                    { _item.name }
                                </span>
                                <span>
                                    { _item?.count }
                                </span>
                            </li>
                        )
                    }
                </ul>
            </div>
        </div>
    </div>
}