import { Children, cloneElement, useState } from "react"

export function FormTabs({ children }) {

    const [active, setActive] = useState(0)
    

    const changeTab = index => () => setActive(index)


    return (
        <div className="tabs">
            <div className="tabs__left">
                <ul className="tabs__left-items">
                    {
                        // TODO: проверять что это компонент является formTab
                        Children.map(children.filter(item => item !== null), (child, index) => {
                            return child ? <li
                                className={ `tabs__left-item ${active === index && 'active'}` }
                                onClick={ changeTab(index) }
                            >
                                <span>{ child?.props.label }</span>
                            </li>
                                : null
                        })
                    }
                </ul>
            </div>
            <div className="tabs__right">
                {
                    Children.map(children.filter(item => item !== null), (child, index) => {
                        return child && <div className={ `tabs__right-item ${active === index && 'active'}` }>
                            {
                                cloneElement(child)
                            }
                        </div>
                    })
                }
            </div>
        </div>
    )
}